use cgmath::{ElementWise, Point2, Vector2};
use std::mem;
use std::num::NonZeroU64;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{
    BindGroupEntry, BindGroupLayoutEntry, BindingResource, BindingType, Buffer, BufferBinding,
    BufferBindingType, BufferUsages, Device, Queue, ShaderStages,
};

const LABEL: Option<&str> = Some("Camera");

/// Camera that defines a view into a map
#[derive(Debug, Copy, Clone, PartialEq)]
#[repr(C)]
pub struct Camera {
    /// Position in map, in tiles
    pub position: Point2<f32>,
    /// Amount of tiles horizontally and vertically at zoom 1
    pub base_dimensions: Vector2<f32>,
    /// Zoom, e.g. (2.0, 0.5) -> twice as many tiles horizontally, half as many vertically
    pub zoom: Vector2<f32>,
    /// Required for webgl compatibility, total struct size must be a multiple of 16
    _padding: [f32; 2],
}

unsafe impl bytemuck::Zeroable for Camera {}
unsafe impl bytemuck::Pod for Camera {}

/// Desired amount of sub-tiles (tiles / 32) in default camera view
pub const AMOUNT: u32 = 1150 * 1000;
/// Maximum amount of sub-tiles horizontally
pub const MAX_WIDTH: u32 = 1500;
/// Maximum amount of sub-tiles vertically
pub const MAX_HEIGHT: u32 = 1150;

/// The aspect ratio is width / height
/// Returns the dimension in pixels that Teeworlds/DDNet would for that aspect ratio
fn aspect_ratio_to_dimensions(aspect_ratio: f32) -> Vector2<f32> {
    /*
    width (x), height (y) calculation from the aspect ratio
        x * y = x * y
    <=>     x = (x * y) / y
    <=>   x^2 = (x * y) * (x / y)
    <=>   x^2 = AMOUNT * aspect_ratio
    <=>     x = sqrt(AMOUNT * aspect_ratio)
    */
    let mut width = (AMOUNT as f32 * aspect_ratio).sqrt();
    // x * y = x * y <=> y = x * (y / x) <=> y = x / aspect_ratio
    let mut height = width / aspect_ratio;
    // If a calculated length exceeds the maximum, cap it at the respective maximum
    if width > MAX_WIDTH as f32 {
        width = MAX_WIDTH as f32;
        height = width / aspect_ratio;
    }
    if height > MAX_HEIGHT as f32 {
        height = MAX_HEIGHT as f32;
        width = height * aspect_ratio;
    }
    Vector2::new(width / 32., height / 32.)
}

impl Camera {
    pub fn new_with_dimensions(base_dimensions: Vector2<f32>) -> Self {
        Self {
            position: Point2::new(0., 0.),
            base_dimensions,
            zoom: Vector2::new(1., 1.),
            _padding: [0.; 2],
        }
    }

    /// The aspect ratio is width / height
    /// Position defaults to (0, 0)
    /// Zoom defaults to (1, 1)
    pub fn new(aspect_ratio: f32) -> Self {
        Self::new_with_dimensions(aspect_ratio_to_dimensions(aspect_ratio))
    }

    /// Supposed to be run whenever the resolution changes
    pub fn switch_aspect_ratio(&mut self, aspect_ratio: f32) {
        self.base_dimensions = aspect_ratio_to_dimensions(aspect_ratio);
    }

    /// Transforms logical coordinates from the camera view into map coordinates **relative** to the camera position
    /// Logical coordinates are (0, 0) at the top-left corner of the camera, (1, 1) at the bottom-right corner
    pub fn relative_map_position(&self, position: Point2<f32>) -> Vector2<f32> {
        // Move (0, 0) to the center of the camera, where the position is defined
        let adjusted_position = position - Point2::new(0.5, 0.5);
        // Relative map position from the center of the camera
        adjusted_position
            .mul_element_wise(self.base_dimensions)
            .mul_element_wise(self.zoom)
    }

    /// Transforms logical coordinates from the camera view into map coordinates
    /// Logical coordinates are (0, 0) at the top-left corner of the camera, (1, 1) at the bottom-right corner
    pub fn map_position(&self, position: Point2<f32>) -> Point2<f32> {
        let relative_position = self.relative_map_position(position);
        // Combine position of camera with the relative position of the point
        self.position + relative_position
    }

    /// Move the camera position so that the specified map_coordinates are at the specified logical coordinates of the camera
    pub fn move_to(&mut self, map_position: Point2<f32>, logical_position: Point2<f32>) {
        let relative_position = self.relative_map_position(logical_position);
        self.position = map_position - relative_position;
    }

    /// Adjust variables for simpler shader calculations
    fn to_gpu(self) -> Self {
        Self {
            base_dimensions: self.base_dimensions / 2.,
            ..self
        }
    }
}

/// Represents a camera which is uploaded to the gpu.
/// Constructed via [`GpuCamera::upload`]
pub struct GpuCamera {
    /// Camera that is currently uploaded to the gpu
    camera: Camera,
    /// Buffer in which the camera lives
    pub buffer: Buffer,
}

impl GpuCamera {
    pub fn upload(camera: &Camera, device: &Device) -> Self {
        let buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::bytes_of(&camera.to_gpu()),
            usage: BufferUsages::UNIFORM | BufferUsages::COPY_DST,
        });
        GpuCamera {
            camera: *camera,
            buffer,
        }
    }

    /// Updates the buffer on the gpu only if the values changed
    pub fn update(&mut self, camera: &Camera, queue: &Queue) {
        if self.camera != *camera {
            queue.write_buffer(&self.buffer, 0, bytemuck::bytes_of(&camera.to_gpu()));
            self.camera = *camera;
        }
    }

    pub fn layout_entry(binding: u32, visibility: ShaderStages) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility,
            ty: BindingType::Buffer {
                ty: BufferBindingType::Uniform,
                has_dynamic_offset: false,
                min_binding_size: Some(NonZeroU64::new(mem::size_of::<Camera>() as u64).unwrap()),
            },
            count: None,
        }
    }

    pub fn entry(&self, binding: u32) -> BindGroupEntry {
        BindGroupEntry {
            binding,
            resource: BindingResource::Buffer(BufferBinding {
                buffer: &self.buffer,
                offset: 0,
                size: None,
            }),
        }
    }
}
