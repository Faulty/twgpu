use twmap::TwMap;
use wgpu::Device;

use super::{GpuGroupRender, GpuMapData, GpuMapStatic};
use crate::{GpuCamera, TwRenderPass};

pub struct GpuMapRender {
    pub groups: Vec<GpuGroupRender>,
}

impl GpuMapStatic {
    pub fn prepare_render(
        &self,
        map: &TwMap,
        data: &GpuMapData,
        camera: &GpuCamera,
        device: &Device,
    ) -> GpuMapRender {
        let groups = map
            .groups
            .iter()
            .zip(data.groups.iter())
            .map(|(group, group_data)| {
                self.prepare_group_render(group, group_data, data, camera, device)
            })
            .collect();
        GpuMapRender { groups }
    }
}

impl GpuMapRender {
    /// Render size parameter passes the width and height of the render target in pixels
    pub fn render<'pass>(&'pass self, render_pass: &mut TwRenderPass<'pass>) {
        let scissor_rect = render_pass.scissor_rect;
        for group in &self.groups {
            render_pass.set_scissor_rect(&scissor_rect);
            group.render(render_pass);
        }
    }
}
