use cgmath::{Point2, Vector2};
use twmap::{Group, Layer, LayerKind};
use wgpu::Device;

use super::{
    GpuGroupData, GpuLayerData, GpuMapData, GpuMapStatic, GpuQuadsRender, GpuTilemapRender,
};
use crate::shared::Clip;
use crate::{GpuCamera, TwRenderPass};

pub enum GpuLayerRender {
    Tilemap(GpuTilemapRender),
    Quads(GpuQuadsRender),
}

pub struct GpuGroupRender {
    pub clip: Option<Clip>,
    pub parallax: Vector2<f32>,
    pub offset: Vector2<f32>,
    pub layers: Vec<GpuLayerRender>,
}

fn is_gpu_layer(layer: &&Layer) -> bool {
    matches!(layer.kind(), LayerKind::Tiles | LayerKind::Quads)
}

impl GpuMapStatic {
    pub fn prepare_group_render(
        &self,
        group: &Group,
        data: &GpuGroupData,
        map_data: &GpuMapData,
        camera: &GpuCamera,
        device: &Device,
    ) -> GpuGroupRender {
        let clip = if group.clipping {
            Some(Clip {
                top_left: Point2::new(group.clip_x, group.clip_y).map(|x| x as f32 / 32.),
                bottom_right: Point2::new(
                    group.clip_x + group.clip_width.max(0),
                    group.clip_y + group.clip_height.max(0),
                )
                .map(|x| x as f32 / 32.),
            })
        } else {
            None
        };
        let parallax = Vector2::new(group.parallax_x, group.parallax_y)
            .cast::<f32>()
            .unwrap()
            / 100.;
        let offset = Vector2::new(group.offset_x, group.offset_y)
            .cast::<f32>()
            .unwrap()
            / 32.
            * -1.; // Group offset is inverted for some reason
        let mut layers = Vec::new();
        for (layer, layer_data) in group
            .layers
            .iter()
            .filter(is_gpu_layer)
            .zip(data.layers.iter())
        {
            match (layer, layer_data) {
                (Layer::Tiles(layer), GpuLayerData::Tilemap(layer_data)) => {
                    layers
                        .push(GpuLayerRender::Tilemap(self.tilemap.prepare_render(
                            layer, layer_data, data, map_data, camera, device,
                        )))
                }
                (Layer::Quads(layer), GpuLayerData::Quads(layer_data)) => {
                    layers
                        .push(GpuLayerRender::Quads(self.quads.prepare_render(
                            layer, layer_data, data, map_data, camera, device,
                        )))
                }
                _ => panic!("Mismatched layers between Group and GroupData"),
            }
        }
        GpuGroupRender {
            clip,
            parallax,
            offset,
            layers,
        }
    }
}

impl GpuLayerRender {
    pub fn render<'pass>(
        &'pass self,
        render_pass: &mut TwRenderPass<'pass>,
        group: &GpuGroupRender,
    ) {
        match self {
            GpuLayerRender::Tilemap(tilemap) => tilemap.render(render_pass, group),
            GpuLayerRender::Quads(quads) => quads.render(render_pass),
        }
    }
}

impl GpuGroupRender {
    pub fn render<'pass>(&'pass self, render_pass: &mut TwRenderPass<'pass>) {
        let scissor_rect = match &self.clip {
            None => render_pass.scissor_rect, // No own clipping
            Some(clip) => {
                match clip.project(render_pass, Vector2::new(1., 1.)) {
                    // Clip of group does not intersect with render target
                    None => return,
                    Some(scissor_rect) => {
                        match scissor_rect.intersect(&render_pass.scissor_rect) {
                            // No intersection between clip and previous scissor rect
                            None => return,
                            Some(scissor_rect) => scissor_rect,
                        }
                    }
                }
            }
        };
        for layer in &self.layers {
            render_pass.set_scissor_rect(&scissor_rect);
            layer.render(render_pass, self);
        }
    }
}
