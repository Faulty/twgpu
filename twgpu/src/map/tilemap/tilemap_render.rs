use cgmath::Point2;
use twmap::TilesLayer;
use wgpu::{
    BindGroupDescriptor, BindGroupEntry, BindingResource, Device, RenderBundle,
    RenderBundleDescriptor, RenderBundleEncoderDescriptor, TextureViewDescriptor,
};

use super::super::{GpuGroupData, GpuMapData, GpuTilemapData, GpuTilemapStatic};
use crate::map::GpuGroupRender;
use crate::shared::Clip;
use crate::{GpuCamera, TwRenderPass};

const LABEL: Option<&str> = Some("Tilemap Render");

pub struct GpuTilemapRender {
    /// When a tile is adjacent to the tilemap border, the clip stretches to infinity in that direction
    /// Contrary to the clip of a group, the bounding box moves with parallax to keep up with the layer
    /// None indicates that there are no visible tiles in the layer
    pub bounding_box: Option<Clip>,
    pub render_bundle: RenderBundle,
}

impl Clip {
    pub fn from_tiles_layer(layer: &TilesLayer) -> Option<Self> {
        let tiles = layer.tiles.unwrap_ref();
        let rows: Vec<_> = tiles.rows().into_iter().collect();
        let top = match rows
            .iter()
            .position(|row| row.iter().any(|tile| tile.id != 0))
        {
            Some(0) => f32::NEG_INFINITY,
            Some(pos) => pos as f32,
            None => return None, // No non-air tiles in layer
        };
        let bottom = match rows
            .iter()
            .rev()
            .position(|row| row.iter().any(|tile| tile.id != 0))
            .unwrap()
        {
            0 => f32::INFINITY,
            pos => (rows.len() - pos) as f32,
        };

        let columns: Vec<_> = tiles.columns().into_iter().collect();
        let left = match columns
            .iter()
            .position(|column| column.iter().any(|tile| tile.id != 0))
            .unwrap()
        {
            0 => f32::NEG_INFINITY,
            pos => pos as f32,
        };
        let right = match columns
            .iter()
            .rev()
            .position(|column| column.iter().any(|tile| tile.id != 0))
            .unwrap()
        {
            0 => f32::INFINITY,
            pos => (columns.len() - pos) as f32,
        };
        Some(Clip {
            top_left: Point2::new(left, top),
            bottom_right: Point2::new(right, bottom),
        })
    }
}

impl GpuTilemapStatic {
    pub fn prepare_render(
        &self,
        layer: &TilesLayer,
        data: &GpuTilemapData,
        group: &GpuGroupData,
        map_data: &GpuMapData,
        camera: &GpuCamera,
        device: &Device,
    ) -> GpuTilemapRender {
        let bounding_box = Clip::from_tiles_layer(layer);
        println!("Bounding box of layer {}: {:?}", layer.name, bounding_box);
        let tilemap_view = data.tilemap.create_view(&TextureViewDescriptor::default());
        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: LABEL,
            layout: &self.bind_group_layout,
            entries: &[
                camera.entry(0),
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::TextureView(&map_data.envelopes.view()),
                },
                BindGroupEntry {
                    binding: 2,
                    resource: BindingResource::TextureView(
                        &map_data.textures.view_array_texture(layer.image),
                    ),
                },
                self.texture_sampler_entry(3),
                BindGroupEntry {
                    binding: 4,
                    resource: BindingResource::TextureView(&tilemap_view),
                },
            ],
        });
        let mut bundle_encoder =
            device.create_render_bundle_encoder(&RenderBundleEncoderDescriptor {
                label: LABEL,
                color_formats: &[Some(self.format)],
                depth_stencil: None,
                sample_count: 1,
                multiview: None,
            });
        bundle_encoder.set_vertex_buffer(1, group.info.slice(..));
        bundle_encoder.set_pipeline(&self.pipeline);
        bundle_encoder.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        bundle_encoder.set_vertex_buffer(2, data.info.slice(..));
        bundle_encoder.set_bind_group(0, &bind_group, &[]);
        bundle_encoder.draw(0..4, 0..1);
        let render_bundle = bundle_encoder.finish(&RenderBundleDescriptor { label: LABEL });
        GpuTilemapRender {
            bounding_box,
            render_bundle,
        }
    }
}

impl GpuTilemapRender {
    pub fn render<'pass>(
        &'pass self,
        render_pass: &mut TwRenderPass<'pass>,
        group: &GpuGroupRender,
    ) {
        if let Some(bounding_box) = &self.bounding_box {
            if let Some(scissor_rect) = bounding_box
                .offset(group.offset)
                .project(render_pass, group.parallax)
            {
                let renderable = render_pass.intersect_scissor_rect(&scissor_rect);
                if renderable {
                    render_pass
                        .render_pass
                        .execute_bundles(std::iter::once(&self.render_bundle));
                }
            }
        }
    }
}
