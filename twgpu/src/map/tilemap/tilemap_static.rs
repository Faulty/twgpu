use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{
    include_wgsl, BindGroupEntry, BindGroupLayout, BindGroupLayoutDescriptor, BindGroupLayoutEntry,
    BindingResource, BindingType, BlendState, Buffer, BufferUsages, ColorTargetState, ColorWrites,
    Device, FragmentState, PipelineLayoutDescriptor, PrimitiveState, PrimitiveTopology,
    RenderPipeline, RenderPipelineDescriptor, Sampler, ShaderStages, TextureFormat,
    TextureSampleType, TextureViewDimension, VertexState,
};

use super::super::{GpuEnvelopesData, GroupInfo, Mapres, TilemapInfo};
use crate::shared::ViewportCorner;
use crate::GpuCamera;

const LABEL: Option<&str> = Some("Tilemap Static");

pub struct GpuTilemapStatic {
    pub format: TextureFormat,
    pub texture_sampler: Sampler,
    pub bind_group_layout: BindGroupLayout,
    pub pipeline: RenderPipeline,
    pub vertex_buffer: Buffer,
}

impl GpuTilemapStatic {
    pub fn tilemap_layout_entry(binding: u32) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility: ShaderStages::VERTEX | ShaderStages::FRAGMENT,
            ty: BindingType::Texture {
                sample_type: TextureSampleType::Float { filterable: false },
                view_dimension: TextureViewDimension::D2,
                multisampled: false,
            },
            count: None,
        }
    }

    pub fn new(format: TextureFormat, device: &Device) -> Self {
        let texture_sampler = Mapres::array_texture_sampler(device);
        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: LABEL,
            entries: &[
                GpuCamera::layout_entry(0, ShaderStages::VERTEX),
                GpuEnvelopesData::layout_entry(1),
                Mapres::texture_layout_entry(2, TextureViewDimension::D3),
                Mapres::sampler_layout_entry(3),
                Self::tilemap_layout_entry(4),
            ],
        });
        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: LABEL,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });
        let shader_module = device.create_shader_module(include_wgsl!("tilemap_shader.wgsl"));
        let vertex_state = VertexState {
            module: &shader_module,
            entry_point: "vs_main",
            buffers: &[
                ViewportCorner::vertex_buffer_layout(),
                GroupInfo::vertex_buffer_layout(),
                TilemapInfo::vertex_buffer_layout(),
            ],
        };
        let fragment_state = FragmentState {
            module: &shader_module,
            entry_point: "fs_main",
            targets: &[Some(ColorTargetState {
                format,
                blend: Some(BlendState::ALPHA_BLENDING),
                write_mask: ColorWrites::all(),
            })],
        };
        let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: LABEL,
            layout: Some(&pipeline_layout),
            vertex: vertex_state,
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleStrip,
                ..PrimitiveState::default()
            },
            depth_stencil: None,
            multisample: Default::default(),
            fragment: Some(fragment_state),
            multiview: None,
        });
        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::cast_slice(&ViewportCorner::TILEMAP),
            usage: BufferUsages::VERTEX,
        });
        GpuTilemapStatic {
            format,
            texture_sampler,
            bind_group_layout,
            pipeline,
            vertex_buffer,
        }
    }

    pub fn texture_sampler_entry(&self, binding: u32) -> BindGroupEntry {
        BindGroupEntry {
            binding,
            resource: BindingResource::Sampler(&self.texture_sampler),
        }
    }
}
