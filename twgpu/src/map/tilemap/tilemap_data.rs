use std::mem;
use twmap::{Tile, TileFlags, TilesLayer};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{
    vertex_attr_array, Buffer, BufferUsages, Device, Extent3d, Queue, Texture, TextureDescriptor,
    TextureDimension, TextureFormat, TextureUsages, TextureView, TextureViewDescriptor,
    VertexAttribute, VertexBufferLayout, VertexStepMode,
};

use super::super::{gpu_color, GpuEnvelopesData};
use crate::{As, To};

const LABEL: Option<&str> = Some("Tilemap Data");

#[derive(Debug, Copy, Clone, bytemuck::Zeroable, bytemuck::Pod)]
#[repr(C)]
pub struct TilemapInfo {
    pub color: [f32; 4],
    pub color_env: i32,
}

impl TilemapInfo {
    const ATTRIBUTES: [VertexAttribute; 2] = vertex_attr_array![6 => Float32x4, 7 => Sint32];

    pub fn vertex_buffer_layout() -> VertexBufferLayout<'static> {
        VertexBufferLayout {
            array_stride: mem::size_of::<TilemapInfo>().AS(),
            step_mode: VertexStepMode::Instance,
            attributes: &Self::ATTRIBUTES,
        }
    }
}

pub struct GpuTilemapData {
    pub tilemap: Texture,
    pub info: Buffer,
}

fn tile_to_gpu(tile: &Tile) -> [f32; 4] {
    let id = (tile.id as f32 + 0.5) / 256.;
    let val_x = tile.flags.contains(TileFlags::FLIP_V).to::<i32>() as f32;
    let val_y = tile.flags.contains(TileFlags::FLIP_H).to::<i32>() as f32;
    let rotate = tile.flags.contains(TileFlags::ROTATE).to::<i32>() as f32;
    [id, val_x, val_y, rotate]
}

impl GpuTilemapData {
    pub fn view(&self) -> TextureView {
        self.tilemap.create_view(&TextureViewDescriptor::default())
    }

    pub fn upload(
        layer: &TilesLayer,
        envelopes: &GpuEnvelopesData,
        device: &Device,
        queue: &Queue,
    ) -> Self {
        let tiles = layer.tiles.unwrap_ref();
        let (height, width) = tiles.dim();
        let data: Vec<f32> = tiles.iter().flat_map(tile_to_gpu).collect();
        let tilemap = device.create_texture_with_data(
            queue,
            &TextureDescriptor {
                label: LABEL,
                size: Extent3d {
                    width: width.AS(),
                    height: height.AS(),
                    depth_or_array_layers: 1,
                },
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba32Float,
                usage: TextureUsages::TEXTURE_BINDING,
            },
            bytemuck::cast_slice(&data),
        );

        let info = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::bytes_of(&TilemapInfo {
                color: gpu_color(layer.color),
                color_env: envelopes.tiles_color_env_index(layer),
            }),
            usage: BufferUsages::VERTEX,
        });
        Self { tilemap, info }
    }
}
