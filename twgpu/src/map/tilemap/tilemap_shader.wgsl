// TODO: Change order of attributes
// TODO: Shouldn't (for example) LayerColor be a uniform buffer

struct ScreenCorner {
    @location(0) position: vec2<f32>,
    @location(1) tex_coords: vec2<f32>,
};

struct GroupInfo {
    @location(4) offset: vec2<f32>,
    @location(5) parallax: vec2<f32>,
};

struct LayerInfo {
    @location(6) color: vec4<f32>,
    @location(7) color_env: i32,
};

struct TilemapFragment {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tilemap_coords: vec2<f32>,
    @location(1) tilemap_bounds: vec2<i32>,
    @location(2) color: vec4<f32>,
};

struct Camera {
    position: vec2<f32>,
    base_dimensions: vec2<f32>, // In tiles
    zoom: vec2<f32>,
    _padding: vec2<f32>,
};

@group(0)
@binding(0)
var<uniform> camera: Camera;
@group(0)
@binding(1)
var envelope_values: texture_1d<f32>;
@group(0)
@binding(4)
// also available in fragment shader
var tilemap: texture_2d<f32>;

@vertex
fn vs_main(corner: ScreenCorner, group_info: GroupInfo, layer_info: LayerInfo) -> TilemapFragment {
    var scaled_tilemap_coords: vec2<f32> = corner.tex_coords * camera.base_dimensions * camera.zoom;
    if (all(group_info.parallax == vec2<f32>(0.))) {
        // Don't zoom on groups with parallax 0
        scaled_tilemap_coords = scaled_tilemap_coords / camera.zoom;
    }
    scaled_tilemap_coords = scaled_tilemap_coords
        + group_info.offset // Group Offset
        + camera.position * group_info.parallax; // Parallaxed Camera Position;
    var frag: TilemapFragment;
    frag.clip_position = vec4<f32>(corner.position, 0.0, 1.0);
    frag.tilemap_coords = scaled_tilemap_coords;
    frag.tilemap_bounds = textureDimensions(tilemap) - vec2<i32>(1, 1);
    frag.color = layer_info.color * textureLoad(envelope_values, layer_info.color_env, 0);
    return frag;
}

@group(0)
@binding(2)
var tex: texture_3d<f32>;
@group(0)
@binding(3)
var tex_sampler: sampler;
// tilemap in binding 4 (see over vertex shader)

@fragment
fn fs_main(frag: TilemapFragment) -> @location(0) vec4<f32> {
    let rounded = vec2<i32>(floor(frag.tilemap_coords));
    let tile_2d_index = clamp(rounded, vec2<i32>(0, 0), frag.tilemap_bounds);
    let tile = textureLoad(tilemap, tile_2d_index, 0);

    var tile_inner_uv = fract(frag.tilemap_coords);
    // tile.w is the rotate bool, so 1 if it should rotate and 0 if not
    // 1 - tile.w is thereby the opposite, so if it shouldn't rotate
    let tex_coords = abs(
        tile.yz - // Tile flipping
        (tile.w * vec2<f32>(tile_inner_uv.y, 1. - tile_inner_uv.x) + (1. - tile.w) * tile_inner_uv) // Tile rotation
    );
    return textureSample(tex, tex_sampler, vec3<f32>(tex_coords, tile.x))
        * frag.color; // Apply tilemap color
}
