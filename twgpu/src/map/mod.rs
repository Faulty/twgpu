//!
//! [`twmap`] is used for the map representation.
//!
//! In this module you will mostly find structs with 3 different postfixes: "Static", "Data" and "Render".
//!
//! Static (`GpuStructStatic`):
//!
//! - Pipelines, Samplers and other things that just have to be initialized once and can be used for multiple instances
//! - Constructed via a `GpuStructStatic::new` function
//! - You should only ever need one instance of this
//! - Note that one instance of this is bound to a [wgpu::TextureFormat], since pipelines can only render to one format
//!
//! Data (`GpuStructData`):
//!
//! - Buffers that have to be uploaded for each instance individually: Vertex/Uniform buffers
//! - Constructed via a `GpuStructData::upload` function
//! - Some have an `update` method, in particular [GpuEnvelopes][map::GpuEnvelopesData]
//!
//! Render (`GpuStructRender`):
//!
//! - Combination of Static and Data and can be used to render to a [crate::TwRenderPass] directly
//! - Includes more data important to the cpu for rendering
//! - Constructed via a method `prepare_render` on the `GpuStructStatic`
//! - Construction is very efficient
//!
//! The main object of this module also falls into this category:
//!
//! - [GpuMapStatic][map::GpuMapStatic] with [GpuMapStatic::new][map::GpuMapStatic::new]
//! - [GpuMapData][map::GpuMapData] with [GpuMapData::upload][map::GpuMapData::upload]
//! - [GpuMapRender][map::GpuMapRender] with [GpuMapStatic::prepare_render][map::GpuMapStatic::prepare_render]
//!
//! Example render code
//! ```no_run
//! # fn main() -> Result<(), twmap::Error> {
//! use twmap::TwMap;
//! use twgpu::{Camera, GpuCamera, TwRenderPass};
//! use twgpu::map::{GpuMapData, GpuMapStatic};
//! # use wgpu::RenderPassDescriptor;
//!
//! # let device: wgpu::Device = unimplemented!();
//! # let queue: wgpu::Queue = unimplemented!();
//! let mut camera = Camera::new(16. / 9.);
//! let mut gpu_camera = GpuCamera::upload(&camera, &device);
//!
//! let map_static = GpuMapStatic::new(wgpu::TextureFormat::Bgra8Unorm, &device);
//! let map = TwMap::parse_path("my_awesome.map")?;
//! let map_data = GpuMapData::upload(&map, &device, &queue);
//! let map_render = map_static.prepare_render(&map, &map_data, &gpu_camera, &device);
//!
//! // Render code that runs every frame
//! # let (client_micros, server_micros) = (21, 2121212121);
//! map_data.envelopes.update(&map.envelopes, client_micros, server_micros, &queue);
//! gpu_camera.update(&camera, &queue);
//! # let some_descriptor = wgpu::CommandEncoderDescriptor { label: Some("Hope you are doing good :)") };
//! let mut command_encoder = device.create_command_encoder(&some_descriptor);
//! {
//!     # let other_descriptor: RenderPassDescriptor = unimplemented!("Writing docs is a little exhausting");
//!     let render_pass = command_encoder.begin_render_pass(&other_descriptor);
//!     let mut tw_render_pass = TwRenderPass::new(render_pass, render_dimensions, &camera);
//!     map_render.render(&mut tw_render_pass);
//! }
//! queue.submit([tw_render_pass]);
//! // Hurray, map is rendered
//! # Ok(())
//! # }
//! ```

mod envelope_interpolation;
mod envelopes;
mod group_data;
mod group_render;
mod map_data;
mod map_render;
mod map_static;
mod mapres;
mod quads;
mod tilemap;

pub use envelope_interpolation::{interpolate, EnvValue, EnvelopeSampling};
pub use envelopes::{GpuEnvelopesData, GpuEnvelopesIndex};
pub use group_data::{gpu_color, GpuGroupData, GpuLayerData, GroupInfo};
pub use group_render::{GpuGroupRender, GpuLayerRender};
pub use map_data::GpuMapData;
pub use map_render::GpuMapRender;
pub use map_static::GpuMapStatic;
pub use mapres::{Mapres, Textures};
pub use quads::{GpuQuadsData, GpuQuadsRender, GpuQuadsStatic, QuadCorner};
pub use tilemap::{GpuTilemapData, GpuTilemapRender, GpuTilemapStatic, TilemapInfo};
