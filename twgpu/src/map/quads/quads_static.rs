use wgpu::{
    include_wgsl, BindGroupLayout, BindGroupLayoutDescriptor, BlendState, ColorTargetState,
    ColorWrites, Device, FragmentState, PipelineLayoutDescriptor, PrimitiveState,
    PrimitiveTopology, RenderPipeline, RenderPipelineDescriptor, Sampler, ShaderStages,
    TextureFormat, TextureViewDimension, VertexState,
};

use super::super::{GpuEnvelopesData, GroupInfo, Mapres, QuadCorner};
use crate::GpuCamera;

const LABEL: Option<&str> = Some("Quads Static");

pub struct GpuQuadsStatic {
    pub format: TextureFormat,
    pub sampler: Sampler,
    pub bind_group_layout: BindGroupLayout,
    pub pipeline: RenderPipeline,
}

impl GpuQuadsStatic {
    pub fn new(format: TextureFormat, device: &Device) -> Self {
        let sampler = Mapres::texture_sampler(device);
        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: LABEL,
            entries: &[
                GpuCamera::layout_entry(0, ShaderStages::VERTEX),
                GpuEnvelopesData::layout_entry(1),
                Mapres::texture_layout_entry(2, TextureViewDimension::D2),
                Mapres::sampler_layout_entry(3),
            ],
        });
        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: LABEL,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });
        let shader_module = device.create_shader_module(include_wgsl!("quad_shader.wgsl"));
        let vertex_state = VertexState {
            module: &shader_module,
            entry_point: "vs_main",
            buffers: &[
                QuadCorner::vertex_buffer_layout(),
                GroupInfo::vertex_buffer_layout(),
            ],
        };
        let fragment_state = FragmentState {
            module: &shader_module,
            entry_point: "fs_main",
            targets: &[Some(ColorTargetState {
                format,
                blend: Some(BlendState::ALPHA_BLENDING),
                write_mask: ColorWrites::all(),
            })],
        };
        let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: LABEL,
            layout: Some(&pipeline_layout),
            vertex: vertex_state,
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleList,
                ..PrimitiveState::default()
            },
            depth_stencil: None,
            multisample: Default::default(),
            fragment: Some(fragment_state),
            multiview: None,
        });
        Self {
            format,
            sampler,
            bind_group_layout,
            pipeline,
        }
    }
}
