use twmap::{Point, Quad, QuadsLayer};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{Buffer, BufferUsages, Device};

use super::super::{gpu_color, GpuEnvelopesData, QuadCorner};
use crate::As;

const LABEL: Option<&str> = Some("Quads Data");

pub struct GpuQuadsData {
    pub vertex_buffer: Buffer,
    pub index_buffer: Buffer,
    pub index_count: u32,
}

fn position_point(point: Point) -> [f32; 2] {
    [point.x as f32 / 1024. / 32., point.y as f32 / 1024. / 32.]
}

fn texture_point(point: Point) -> [f32; 2] {
    [point.x as f32 / 1024., point.y as f32 / 1024.]
}

impl QuadCorner {
    fn split_quad(quad: &Quad, envelopes: &GpuEnvelopesData) -> [Self; 4] {
        (0..4)
            .map(|i| {
                let position = position_point(quad.corners[i]);
                let center = position_point(quad.position);
                let offset = [position[0] - center[0], position[1] - center[1]];
                Self {
                    center_offset: [center[0], center[1], offset[0], offset[1]],
                    tex_coords: texture_point(quad.texture_coords[i]),
                    color: gpu_color(quad.colors[i]),
                    env_indices: [
                        envelopes.quad_color_env_index(quad),
                        envelopes.quad_position_env_index(quad),
                    ],
                }
            })
            .collect::<Vec<_>>()
            .try_into()
            .unwrap()
    }
}

impl GpuQuadsData {
    pub fn upload(layer: &QuadsLayer, envelopes: &GpuEnvelopesData, device: &Device) -> Self {
        let mut vertices = Vec::new();
        let mut indices: Vec<u32> = Vec::new();
        for (n, quad) in layer.quads.iter().enumerate() {
            vertices.extend(QuadCorner::split_quad(quad, envelopes));
            indices.extend([0, 1, 3, 0, 2, 3].map(|i| i + n.AS::<u32>() * 4));
        }
        let vertex_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::cast_slice(vertices.as_slice()),
            usage: BufferUsages::VERTEX,
        });
        let index_buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::cast_slice(indices.as_slice()),
            usage: BufferUsages::INDEX,
        });
        let index_count = indices.len().AS::<u32>();
        Self {
            vertex_buffer,
            index_buffer,
            index_count,
        }
    }
}
