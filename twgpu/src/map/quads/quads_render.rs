use twmap::QuadsLayer;
use wgpu::{
    BindGroupDescriptor, BindGroupEntry, BindingResource, Device, IndexFormat, RenderBundle,
    RenderBundleDescriptor, RenderBundleEncoderDescriptor,
};

use super::super::{GpuGroupData, GpuMapData, GpuQuadsData, GpuQuadsStatic};
use crate::{GpuCamera, TwRenderPass};

const LABEL: Option<&str> = Some("Quads Render");

pub struct GpuQuadsRender {
    pub render_bundle: RenderBundle,
}

impl GpuQuadsStatic {
    pub fn prepare_render(
        &self,
        layer: &QuadsLayer,
        data: &GpuQuadsData,
        group: &GpuGroupData,
        map_data: &GpuMapData,
        camera: &GpuCamera,
        device: &Device,
    ) -> GpuQuadsRender {
        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: LABEL,
            layout: &self.bind_group_layout,
            entries: &[
                camera.entry(0),
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::TextureView(&map_data.envelopes.view()),
                },
                BindGroupEntry {
                    binding: 2,
                    resource: BindingResource::TextureView(
                        &map_data.textures.view_texture(layer.image),
                    ),
                },
                BindGroupEntry {
                    binding: 3,
                    resource: BindingResource::Sampler(&self.sampler),
                },
            ],
        });
        let mut bundle_encoder =
            device.create_render_bundle_encoder(&RenderBundleEncoderDescriptor {
                label: LABEL,
                color_formats: &[Some(self.format)],
                depth_stencil: None,
                sample_count: 1,
                multiview: None,
            });
        bundle_encoder.set_vertex_buffer(1, group.info.slice(..));
        bundle_encoder.set_pipeline(&self.pipeline);
        bundle_encoder.set_vertex_buffer(0, data.vertex_buffer.slice(..));
        bundle_encoder.set_index_buffer(data.index_buffer.slice(..), IndexFormat::Uint32);
        bundle_encoder.set_bind_group(0, &bind_group, &[]);
        bundle_encoder.draw_indexed(0..data.index_count, 0, 0..1);
        let render_bundle = bundle_encoder.finish(&RenderBundleDescriptor { label: LABEL });
        GpuQuadsRender { render_bundle }
    }
}

impl GpuQuadsRender {
    pub fn render<'pass>(&'pass self, render_pass: &mut TwRenderPass<'pass>) {
        render_pass
            .render_pass
            .execute_bundles(std::iter::once(&self.render_bundle));
    }
}
