use twmap::TwMap;
use wgpu::{Device, Queue};

use super::{GpuEnvelopesData, GpuGroupData, Textures};

pub struct GpuMapData {
    pub textures: Textures,
    pub envelopes: GpuEnvelopesData,
    pub groups: Vec<GpuGroupData>,
}

impl GpuMapData {
    pub fn upload(map: &TwMap, device: &Device, queue: &Queue) -> GpuMapData {
        let textures = Textures::upload(map, device, queue);
        let envelopes = GpuEnvelopesData::upload(map, device);
        let groups = map
            .groups
            .iter()
            .map(|group| GpuGroupData::upload(group, &envelopes, device, queue))
            .collect();
        GpuMapData {
            textures,
            envelopes,
            groups,
        }
    }
}
