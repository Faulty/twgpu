use crate::{As, To};
use std::f32::consts;
use std::ops::{Add, Mul, Sub};
use twmap::{Color, CurveKind, Env, Envelope, I32Color, Position};

/// Pretty much just a reimplementation of `Vector4<f32>`
/// TODO: Remove this as soon as Vector4 derives the bytemuck traits
#[derive(Debug, Copy, Clone, bytemuck::Zeroable, bytemuck::Pod)]
#[repr(C)]
pub struct EnvValue([f32; 4]);

impl EnvValue {
    /// Neutral element of multiplication
    pub const NEUTRAL_COLOR: Self = Self([1., 1., 1., 1.]);
    /// Neutral element of addition
    pub const NEUTRAL_POSITION: Self = Self([0., 0., 0., 0.]);
}

impl Add<EnvValue> for EnvValue {
    type Output = EnvValue;

    fn add(self, rhs: EnvValue) -> Self::Output {
        EnvValue([
            self.0[0] + rhs.0[0],
            self.0[1] + rhs.0[1],
            self.0[2] + rhs.0[2],
            self.0[3] + rhs.0[3],
        ])
    }
}

impl Sub<EnvValue> for EnvValue {
    type Output = EnvValue;

    fn sub(self, rhs: EnvValue) -> Self::Output {
        EnvValue([
            self.0[0] - rhs.0[0],
            self.0[1] - rhs.0[1],
            self.0[2] - rhs.0[2],
            self.0[3] - rhs.0[3],
        ])
    }
}

impl Mul<f32> for EnvValue {
    type Output = EnvValue;

    fn mul(self, rhs: f32) -> Self::Output {
        EnvValue([
            self.0[0] * rhs,
            self.0[1] * rhs,
            self.0[2] * rhs,
            self.0[3] * rhs,
        ])
    }
}

impl Mul<EnvValue> for EnvValue {
    type Output = EnvValue;

    fn mul(self, rhs: EnvValue) -> Self::Output {
        EnvValue([
            self.0[0] * rhs.0[0],
            self.0[1] * rhs.0[1],
            self.0[2] * rhs.0[2],
            self.0[3] * rhs.0[3],
        ])
    }
}

fn env_val(val: i32) -> f32 {
    val as f32 / 1024.
}

impl From<i32> for EnvValue {
    fn from(val: i32) -> Self {
        EnvValue([env_val(val), 0., 0., 0.])
    }
}

impl From<Position> for EnvValue {
    fn from(pos: Position) -> Self {
        EnvValue([
            env_val(pos.x) / 32.,
            env_val(pos.y) / 32.,
            env_val(pos.rotation) / 180. * consts::PI,
            0.,
        ])
    }
}

impl From<I32Color> for EnvValue {
    fn from(c: I32Color) -> Self {
        EnvValue([c.r, c.g, c.b, c.a].map(env_val))
    }
}

impl From<Color> for EnvValue {
    fn from(c: Color) -> Self {
        EnvValue([c.r, c.g, c.b, c.a].map(|val| val as f32 / u8::MAX as f32))
    }
}

pub fn interpolate<T>(frac: f32, curve: CurveKind<T>) -> f32 {
    use CurveKind::*;
    match curve {
        Step => 0.,
        Linear => frac,
        Slow => frac.powi(3),
        Fast => 1. - (1. - frac).powi(3),
        // TODO: Implement Bezier curves
        Smooth | Bezier(_) => 3. * frac.powi(2) - 2. * frac.powi(3),
        Unknown(_) => 0.5,
    }
}

pub trait EnvelopeSampling {
    fn sample(&self, client_micros: i64, server_micros: i64, offset: i32) -> EnvValue;
}

impl<T: Copy + Into<EnvValue>> EnvelopeSampling for Env<T> {
    fn sample(&self, client_micros: i64, server_micros: i64, offset: i32) -> EnvValue {
        match self.points.len() {
            0 => return EnvValue([0., 0., 0., 0.]),
            1 => return self.points.first().unwrap().content.into(),
            _ => {}
        };
        let mut micros: i64 = match self.synchronized {
            true => server_micros,
            false => client_micros,
        };
        micros += offset.to::<i64>() * 1000;
        micros %= self.points.last().unwrap().time.to::<i64>() * 1000;
        let millis = (micros / 1000).AS::<i32>();
        if self.points[0].time > millis {
            return self.points.last().unwrap().content.into();
        }
        let interpolation_points = self
            .points
            .windows(2)
            .find(|tuple| tuple[0].time <= millis && tuple[1].time >= millis)
            .unwrap();
        let left_micros = interpolation_points[0].time.to::<i64>() * 1000;
        let right_micros = interpolation_points[1].time.to::<i64>() * 1000;
        let time_span_micros = right_micros - left_micros;
        let time_span_moment = micros - left_micros;
        let frac = time_span_moment as f32 / time_span_micros as f32;
        let left = interpolation_points[0].content.into();
        let right = interpolation_points[1].content.into();
        left + (right - left) * interpolate(frac, interpolation_points[0].curve)
    }
}

impl EnvelopeSampling for Envelope {
    fn sample(&self, client_micros: i64, server_micros: i64, offset: i32) -> EnvValue {
        use Envelope::*;
        match self {
            Position(env) => env.sample(client_micros, server_micros, offset),
            Color(env) => env.sample(client_micros, server_micros, offset),
            Sound(env) => env.sample(client_micros, server_micros, offset),
        }
    }
}
