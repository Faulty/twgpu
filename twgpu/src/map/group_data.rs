use std::mem;
use twmap::{Color, Group, Layer};
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{
    vertex_attr_array, Buffer, BufferUsages, Device, Queue, VertexAttribute, VertexBufferLayout,
    VertexStepMode,
};

use super::{GpuEnvelopesData, GpuQuadsData, GpuTilemapData};
use crate::As;

const LABEL: Option<&str> = Some("Group");

pub fn gpu_color(color: Color) -> [f32; 4] {
    [color.r, color.g, color.b, color.a].map(|c| c as f32 / u8::MAX as f32)
}

#[derive(Debug, Copy, Clone, bytemuck::Zeroable, bytemuck::Pod)]
#[repr(C)]
/// Struct contained in the vertex buffer
pub struct GroupInfo {
    offset: [f32; 2],
    parallax: [f32; 2],
}

impl GroupInfo {
    const ATTRIBUTES: [VertexAttribute; 2] = vertex_attr_array![4 => Float32x2, 5 => Float32x2];

    pub fn vertex_buffer_layout() -> VertexBufferLayout<'static> {
        VertexBufferLayout {
            array_stride: mem::size_of::<GroupInfo>().AS(),
            step_mode: VertexStepMode::Instance,
            attributes: &Self::ATTRIBUTES,
        }
    }
}

pub enum GpuLayerData {
    Tilemap(GpuTilemapData),
    Quads(GpuQuadsData),
}

pub struct GpuGroupData {
    pub info: Buffer,
    pub layers: Vec<GpuLayerData>,
}

impl GpuGroupData {
    pub fn upload(
        group: &Group,
        envelopes: &GpuEnvelopesData,
        device: &Device,
        queue: &Queue,
    ) -> Self {
        let info = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::cast_slice(&[GroupInfo {
                offset: [group.offset_x as f32 / 32., group.offset_y as f32 / 32.],
                parallax: [
                    group.parallax_x as f32 / 100.,
                    group.parallax_y as f32 / 100.,
                ],
            }]),
            usage: BufferUsages::VERTEX,
        });
        let mut layers = Vec::new();
        for layer in &group.layers {
            if let Some(gpu_layer) = match layer {
                Layer::Tiles(layer) => Some(GpuLayerData::Tilemap(GpuTilemapData::upload(
                    layer, envelopes, device, queue,
                ))),
                Layer::Quads(layer) => Some(GpuLayerData::Quads(GpuQuadsData::upload(
                    layer, envelopes, device,
                ))),
                _ => None,
            } {
                layers.push(gpu_layer);
            }
        }
        Self { info, layers }
    }
}
