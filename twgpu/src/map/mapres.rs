use crate::As;
use image::{Rgba, RgbaImage};
use std::iter;
use twmap::{Image, Layer, TwMap};
use wgpu::util::DeviceExt;
use wgpu::{
    AddressMode, BindGroupLayoutEntry, BindingType, CommandEncoderDescriptor, Device, Extent3d,
    FilterMode, ImageCopyTexture, Origin3d, Queue, Sampler, SamplerBindingType, SamplerDescriptor,
    ShaderStages, Texture, TextureAspect, TextureDescriptor, TextureDimension, TextureFormat,
    TextureSampleType, TextureUsages, TextureView, TextureViewDescriptor, TextureViewDimension,
};

const LABEL: Option<&str> = Some("Mapres");

pub struct Mapres {
    pub name: String,
    pub width: u32,
    pub height: u32,
    pub texture: Texture,
    pub array_texture: Option<Texture>,
}

/// Contains all textures used in a map
pub struct Textures {
    /// All mapres contained in the map, in order
    pub mapres: Vec<Mapres>,
    /// White texture used when a layer doesn't specify an image
    pub blank: Mapres,
}

impl Mapres {
    /// Sampler for quad rendering
    pub fn texture_sampler(device: &Device) -> Sampler {
        device.create_sampler(&SamplerDescriptor {
            label: LABEL,
            address_mode_u: AddressMode::Repeat,
            address_mode_v: AddressMode::Repeat,
            mag_filter: FilterMode::Linear,
            min_filter: FilterMode::Linear,
            ..SamplerDescriptor::default()
        })
    }

    /// Sampler for tilemap rendering
    pub fn array_texture_sampler(device: &Device) -> Sampler {
        device.create_sampler(&SamplerDescriptor {
            label: LABEL,
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            mag_filter: FilterMode::Linear,
            min_filter: FilterMode::Linear,
            ..SamplerDescriptor::default()
        })
    }

    pub fn sampler_layout_entry(binding: u32) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility: ShaderStages::FRAGMENT,
            ty: BindingType::Sampler(SamplerBindingType::Filtering),
            count: None,
        }
    }

    pub fn texture_layout_entry(
        binding: u32,
        view_dimension: TextureViewDimension,
    ) -> BindGroupLayoutEntry {
        BindGroupLayoutEntry {
            binding,
            visibility: ShaderStages::FRAGMENT,
            ty: BindingType::Texture {
                sample_type: TextureSampleType::Float { filterable: true },
                view_dimension,
                multisampled: false,
            },
            count: None,
        }
    }

    pub fn from_rgba(image: &RgbaImage, name: String, device: &Device, queue: &Queue) -> Self {
        let width = image.width();
        let height = image.height();
        let texture = device.create_texture_with_data(
            queue,
            &TextureDescriptor {
                label: Some(&format!("Image {}", name)),
                size: Extent3d {
                    width,
                    height,
                    depth_or_array_layers: 1,
                },
                mip_level_count: 1,
                sample_count: 1,
                dimension: TextureDimension::D2,
                format: TextureFormat::Rgba8Unorm,
                usage: TextureUsages::TEXTURE_BINDING
                    | TextureUsages::COPY_SRC
                    | TextureUsages::COPY_DST,
            },
            image.as_raw(),
        );
        Self {
            name,
            width,
            height,
            texture,
            array_texture: None,
        }
    }

    /// Generates the array texture for this mapres if it isn't already generated
    pub fn generate_array_texture(&mut self, device: &Device, queue: &Queue) {
        if self.array_texture.is_some() {
            return;
        }
        assert_eq!(self.width % 16, 0);
        assert_eq!(self.height % 16, 0);
        let tile_width = self.width / 16;
        let tile_height = self.height / 16;
        let array_texture = device.create_texture(&TextureDescriptor {
            label: Some(&format!("Array Image {}", self.name)),
            size: Extent3d {
                width: tile_width,
                height: tile_height,
                depth_or_array_layers: 256,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D3,
            format: TextureFormat::Rgba8Unorm,
            usage: TextureUsages::TEXTURE_BINDING
                | TextureUsages::COPY_DST
                | TextureUsages::COPY_SRC,
        });
        let mut command_encoder = device.create_command_encoder(&CommandEncoderDescriptor {
            label: Some("Array Texture Creator"),
        });
        let copy_size = Extent3d {
            width: tile_width,
            height: tile_height,
            depth_or_array_layers: 1,
        };

        // TODO: Remove this workaround
        // The array texture breaks when I don't write to the first layer
        // When fixing this, also remove TextureUsages::COPY_SRC from the array texture
        command_encoder.copy_texture_to_texture(
            ImageCopyTexture {
                texture: &array_texture,
                mip_level: 0,
                origin: Origin3d { x: 0, y: 0, z: 1 },
                aspect: TextureAspect::All,
            },
            ImageCopyTexture {
                texture: &array_texture,
                mip_level: 0,
                origin: Origin3d { x: 0, y: 0, z: 0 },
                aspect: TextureAspect::All,
            },
            copy_size,
        );

        for y in 0..16 {
            for x in 0..16 {
                if (x, y) == (0, 0) {
                    continue;
                }
                command_encoder.copy_texture_to_texture(
                    ImageCopyTexture {
                        texture: &self.texture,
                        mip_level: 0,
                        origin: Origin3d {
                            x: x * tile_width,
                            y: y * tile_height,
                            z: 0,
                        },
                        aspect: TextureAspect::All,
                    },
                    ImageCopyTexture {
                        texture: &array_texture,
                        mip_level: 0,
                        origin: Origin3d {
                            x: 0,
                            y: 0,
                            z: y * 16 + x,
                        },
                        aspect: TextureAspect::All,
                    },
                    copy_size,
                );
            }
        }
        self.array_texture = Some(array_texture);
        queue.submit(iter::once(command_encoder.finish()));
    }

    /// Accessor to the array texture of this mapres
    /// Panics it `generate_array_texture` wasn't called previously
    pub fn array_texture(&self) -> &Texture {
        match &self.array_texture {
            None => {
                panic!("Accessed array texture was not generated, use `generate_array_texture`")
            }
            Some(texture) => texture,
        }
    }
}

impl Textures {
    pub fn upload(map: &TwMap, device: &Device, queue: &Queue) -> Self {
        let mut mapres = Vec::new();
        for image in &map.images {
            mapres.push(match image {
                Image::External(_) => panic!("External images can't be rendered, embed them!"),
                Image::Embedded(emb) => {
                    Mapres::from_rgba(emb.image.unwrap_ref(), image.name().clone(), device, queue)
                }
            })
        }
        let blank = Mapres::from_rgba(
            &RgbaImage::from_pixel(16, 16, Rgba([255; 4])),
            "Blank Image".into(),
            device,
            queue,
        );
        let mut mapres = Self { mapres, blank };
        for group in &map.groups {
            for layer in &group.layers {
                if let Layer::Tiles(layer) = layer {
                    mapres.generate_array_texture(layer.image, device, queue);
                }
            }
        }
        mapres
    }

    pub fn view_texture(&self, index: Option<u16>) -> TextureView {
        match index {
            None => self
                .blank
                .texture
                .create_view(&TextureViewDescriptor::default()),
            Some(index) => self.mapres[index.AS::<usize>()]
                .texture
                .create_view(&TextureViewDescriptor::default()),
        }
    }

    pub fn generate_array_texture(&mut self, index: Option<u16>, device: &Device, queue: &Queue) {
        match index {
            None => self.blank.generate_array_texture(device, queue),
            Some(index) => self.mapres[index.AS::<usize>()].generate_array_texture(device, queue),
        }
    }

    /// Generates array texture on first use
    pub fn view_array_texture(&self, index: Option<u16>) -> TextureView {
        match index {
            None => self
                .blank
                .array_texture()
                .create_view(&TextureViewDescriptor::default()),
            Some(index) => self.mapres[index.AS::<usize>()]
                .array_texture()
                .create_view(&TextureViewDescriptor::default()),
        }
    }
}
