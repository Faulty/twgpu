use crate::As;
use cgmath::{ElementWise, EuclideanSpace, Point2, Vector2};
use std::mem;
use wgpu::{vertex_attr_array, RenderPass, VertexAttribute, VertexBufferLayout, VertexStepMode};

use crate::TwRenderPass;

/// There is no generic implementation because `f32` does not implement `Ord`
/// Adding a generic implementation would cause an error
pub trait ClampElementWise {
    fn clamp_element_wise(self, min: Self, max: Self) -> Self;
}

impl ClampElementWise for Vector2<f32> {
    fn clamp_element_wise(self, min: Self, max: Self) -> Self {
        Vector2::new(self.x.clamp(min.x, max.x), self.y.clamp(min.y, max.y))
    }
}

/// One dimensional line, used for [`ScissorRect`]
#[derive(Debug, Copy, Clone)]
pub struct Line {
    pub position: u32,
    pub length: u32,
}

impl Line {
    /// Returns `None` if the two lines do not intersect
    pub fn intersect(self, other: Line) -> Option<Self> {
        let (lower, upper) = if self.position < other.position {
            (self, other)
        } else {
            (other, self)
        };
        if upper.position >= lower.position + lower.length {
            None
        } else {
            Some(Line {
                position: upper.position,
                length: upper
                    .length
                    .min(lower.position + lower.length - upper.position),
            })
        }
    }
}

/// Defines a rectangle that can be used to limit the rendering area
#[derive(Debug, Copy, Clone)]
pub struct ScissorRect {
    pub x: Line,
    pub y: Line,
}

impl ScissorRect {
    /// Returns a ScissorRect that covers the entire viewport
    pub fn viewport(size: Vector2<u32>) -> Self {
        // TODO: Return Option/None if size is 0 in one direction?
        Self {
            x: Line {
                position: 0,
                length: size.x,
            },
            y: Line {
                position: 0,
                length: size.y,
            },
        }
    }

    /// Assumes a coordinate system alignment with (0, 0) being the top-right corner
    /// This is the case for Teeworlds/DDNet maps
    pub fn from_corners(top_left: Point2<u32>, bottom_right: Point2<u32>) -> Option<Self> {
        if top_left.x == bottom_right.x || top_left.y == bottom_right.y {
            None
        } else {
            Some(Self {
                x: Line {
                    position: top_left.x,
                    length: bottom_right.x - top_left.x + 1,
                },
                y: Line {
                    position: top_left.y,
                    length: bottom_right.y - top_left.y + 1,
                },
            })
        }
    }

    /// Returns the area of the viewport that both `ScissorRect`s cover
    /// If they do not intersect, None is returned
    pub fn intersect(&self, other: &Self) -> Option<Self> {
        Some(Self {
            x: self.x.intersect(other.x)?,
            y: self.y.intersect(other.y)?,
        })
    }

    pub fn apply(&self, render_pass: &mut RenderPass) {
        render_pass.set_scissor_rect(
            self.x.position,
            self.y.position,
            self.x.length,
            self.y.length,
        );
    }
}

/// An area in map coordinates, tiles as the unit
/// Specifies that rendering should be limited to the rectangle
#[derive(Debug, Copy, Clone)]
pub struct Clip {
    pub top_left: Point2<f32>,
    pub bottom_right: Point2<f32>,
}

const ZERO: Vector2<f32> = Vector2::new(0., 0.);
const ONE: Vector2<f32> = Vector2::new(1., 1.);

impl Clip {
    /// Returns the area of the viewport that is part of the render-area with the clip active
    pub fn project(
        &self,
        render_pass: &TwRenderPass,
        parallax: Vector2<f32>,
    ) -> Option<ScissorRect> {
        // The amount of tiles vertically and horizontally across the screen
        let mut tile_counts = render_pass.camera.base_dimensions;
        // Parallax == 0 is special-cased to not include zoom
        if parallax != ZERO {
            tile_counts = tile_counts.mul_element_wise(render_pass.camera.zoom);
        }

        let render_size = render_pass.size.cast::<f32>().unwrap();

        // Position of the camera from the clip's coordinate system (parallax is calculated into position of camera)
        let camera_position = render_pass
            .camera
            .position
            .mul_element_wise(Point2::from_vec(parallax));
        // Since we want to calculate the area of the viewport that is covered by the clip,
        // we want to go into a coordinate system where the top-left corner of what the camera sees to be our coordinate system's origin
        // Lets call it the 'viewport' coordinate system
        let camera_top_left = camera_position - tile_counts / 2.;

        let clip_top_left = Point2::from_vec(
            (self.top_left - camera_top_left) // Move into viewport coordinates
                .div_element_wise(tile_counts) // Normalize viewport coordinate system so that (1, 1) is the bottom-right corner of the viewport
                .mul_element_wise(render_size) // Change viewport coordinate system so that (viewport_width, viewport_height) is the bottom-right corner of the viewport
                .clamp_element_wise(ZERO, render_size - ONE) // Clamp the coordinates into the viewport
                .cast::<u32>()
                .unwrap(),
        );
        let clip_bottom_right = Point2::from_vec(
            (self.bottom_right - camera_top_left)
                .div_element_wise(tile_counts)
                .mul_element_wise(render_size)
                .clamp_element_wise(ZERO, render_size - ONE)
                .cast::<u32>()
                .unwrap(),
        );
        ScissorRect::from_corners(clip_top_left, clip_bottom_right)
    }

    pub fn offset(mut self, offset: Vector2<f32>) -> Self {
        self.top_left += offset;
        self.bottom_right += offset;
        self
    }
}

#[derive(Debug, Copy, Clone, bytemuck::Zeroable, bytemuck::Pod)]
#[repr(C)]
/// Used for shaders that cover the entire screen
pub struct ViewportCorner {
    pub position: [f32; 2],
    pub tex_coords: [f32; 2],
}

impl ViewportCorner {
    pub const ATTRIBUTES: [VertexAttribute; 2] = vertex_attr_array![0 => Float32x2, 1 => Float32x2];

    pub fn vertex_buffer_layout() -> VertexBufferLayout<'static> {
        VertexBufferLayout {
            array_stride: mem::size_of::<Self>().AS(),
            step_mode: VertexStepMode::Vertex,
            attributes: &Self::ATTRIBUTES,
        }
    }

    pub const TILEMAP: [Self; 4] = [
        // Top-left
        Self {
            position: [-1., 1.],
            tex_coords: [-1., -1.],
        },
        // Top-right
        Self {
            position: [1., 1.],
            tex_coords: [1., -1.],
        },
        // Bottom-left
        Self {
            position: [-1., -1.],
            tex_coords: [-1., 1.],
        },
        // Bottom-right
        Self {
            position: [1., -1.],
            tex_coords: [1., 1.],
        },
    ];

    pub const TEXTURE: [Self; 4] = [
        // Top-left
        Self {
            position: [-1., 1.],
            tex_coords: [0., 0.],
        },
        // Top-right
        Self {
            position: [1., 1.],
            tex_coords: [1., 0.],
        },
        // Bottom-left
        Self {
            position: [-1., -1.],
            tex_coords: [0., 1.],
        },
        // Bottom-right
        Self {
            position: [1., -1.],
            tex_coords: [1., 1.],
        },
    ];
}
