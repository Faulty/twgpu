use crate::shared::ViewportCorner;
use wgpu::util::{BufferInitDescriptor, DeviceExt};
use wgpu::{
    include_wgsl, AddressMode, BindGroup, BindGroupDescriptor, BindGroupEntry, BindGroupLayout,
    BindGroupLayoutDescriptor, BindGroupLayoutEntry, BindingResource, BindingType, Buffer,
    BufferUsages, ColorTargetState, ColorWrites, CommandEncoder, Device, Extent3d, FragmentState,
    MultisampleState, Operations, PipelineLayoutDescriptor, PrimitiveState, PrimitiveTopology,
    RenderPassColorAttachment, RenderPassDescriptor, RenderPipeline, RenderPipelineDescriptor,
    Sampler, SamplerBindingType, SamplerDescriptor, ShaderStages, Surface, SurfaceConfiguration,
    SurfaceError, SurfaceTexture, Texture, TextureDescriptor, TextureDimension, TextureSampleType,
    TextureUsages, TextureViewDescriptor, TextureViewDimension, VertexState,
};

pub struct IndirectRgbaSurface {
    config: SurfaceConfiguration,
    intermediate: Texture,

    pipeline: RenderPipeline,
    buffer: Buffer,
    sampler: Sampler,
    bind_group: BindGroup,
    bind_group_layout: BindGroupLayout,
}

/// Abstraction over surfaces of all texture formats, to be able to render to them all as if they weren't srgb.
/// The interface of this struct is very similar to that of a [wgpu::Surface]
///
/// Example code:
/// ```no_run
/// use wgpu::TextureViewDescriptor;
/// use twgpu::RgbaSurface;
/// # let device: wgpu::Device = unimplemented!();
/// # let queue: wgpu::Queue = unimplemented!();
/// # let surface: wgpu::Surface = unimplemented!();
/// # let surface_configuration: wgpu::SurfaceConfiguration = unimplemented!();
/// let mut rgba_surface = RgbaSurface::new(surface, surface_configuration.clone(), &device);
///
/// // Reconfiguration (for example resolution change)
/// rgba_surface.configure(surface_configuration.clone(), &device);
///
/// // Code that runs each frame
/// if let Ok(texture) = rgba_surface.get_current_texture() {
///     let texture_view = texture.texture().create_view(&TextureViewDescriptor::default());
///     # let descriptor = wgpu::CommandEncoderDescriptor { label: Some("Serious Encoding") };
///     let mut command_encoder = device.create_command_encoder(&descriptor);
///     // Create RenderPass with `texture_view` here
///     // Do some nice rendering
///     if let Ok(surface_texture) = texture.prepare_present(&rgba_surface, &mut command_encoder) {
///         queue.submit([command_encoder.finish()]);
///         surface_texture.present();
///     }
/// }
/// ```
pub struct RgbaSurface {
    surface: Surface,
    indirect: Option<IndirectRgbaSurface>,
}

pub enum RgbaSurfaceTexture<'a> {
    Direct(SurfaceTexture),
    Indirect(&'a Texture),
}

const LABEL: Option<&str> = Some("Rgba Surface");

impl RgbaSurfaceTexture<'_> {
    pub fn texture(&self) -> &Texture {
        match self {
            RgbaSurfaceTexture::Direct(st) => &st.texture,
            RgbaSurfaceTexture::Indirect(t) => t,
        }
    }

    pub fn prepare_present(
        self,
        surface: &RgbaSurface,
        command_encoder: &mut CommandEncoder,
    ) -> Result<SurfaceTexture, SurfaceError> {
        match (self, &surface.indirect) {
            (RgbaSurfaceTexture::Direct(st), None) => Ok(st),
            (RgbaSurfaceTexture::Indirect(_), Some(indirect)) => {
                let surface_texture = surface.surface.get_current_texture()?;
                let surface_texture_view = &surface_texture
                    .texture
                    .create_view(&TextureViewDescriptor::default());
                {
                    let mut render_pass =
                        command_encoder.begin_render_pass(&RenderPassDescriptor {
                            label: LABEL,
                            color_attachments: &[Some(RenderPassColorAttachment {
                                view: surface_texture_view,
                                resolve_target: None,
                                ops: Operations::default(),
                            })],
                            depth_stencil_attachment: None,
                        });
                    render_pass.set_pipeline(&indirect.pipeline);
                    render_pass.set_vertex_buffer(0, indirect.buffer.slice(..));
                    render_pass.set_bind_group(0, &indirect.bind_group, &[]);
                    render_pass.draw(0..4, 0..1);
                }
                Ok(surface_texture)
            }
            _ => panic!("Mismatch between direct/indirectness of surface texture"),
        }
    }
}

impl RgbaSurface {
    pub fn get_current_texture(&self) -> Result<RgbaSurfaceTexture, SurfaceError> {
        match &self.indirect {
            None => self
                .surface
                .get_current_texture()
                .map(RgbaSurfaceTexture::Direct),
            Some(indirect) => Ok(RgbaSurfaceTexture::Indirect(&indirect.intermediate)),
        }
    }

    pub fn configure(&mut self, config: SurfaceConfiguration, device: &Device) {
        self.surface.configure(device, &config);
        match &mut self.indirect {
            None => match config.format.describe().srgb {
                true => self.indirect = Some(IndirectRgbaSurface::new(config, device)),
                false => {}
            },
            Some(indirect) => match config.format.describe().srgb {
                false => self.indirect = None,
                true => indirect.configure(config, device),
            },
        }
    }

    pub fn new(surface: Surface, config: SurfaceConfiguration, device: &Device) -> Self {
        surface.configure(device, &config);
        let indirect = match config.format.describe().srgb {
            false => None,
            true => Some(IndirectRgbaSurface::new(config, device)),
        };
        Self { surface, indirect }
    }
}

impl IndirectRgbaSurface {
    fn configure(&mut self, config: SurfaceConfiguration, device: &Device) {
        assert!(config.format.describe().srgb);
        if config.width != self.config.width || config.height != self.config.height {
            let (intermediate, bind_group) =
                Self::intermediate(&config, &self.bind_group_layout, &self.sampler, device);
            self.intermediate = intermediate;
            self.bind_group = bind_group;
            self.config = config;
        }
    }
    fn intermediate(
        config: &SurfaceConfiguration,
        layout: &BindGroupLayout,
        sampler: &Sampler,
        device: &Device,
    ) -> (Texture, BindGroup) {
        let intermediate = device.create_texture(&TextureDescriptor {
            label: LABEL,
            size: Extent3d {
                width: config.width,
                height: config.height,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: TextureDimension::D2,
            format: config.format,
            usage: TextureUsages::RENDER_ATTACHMENT | TextureUsages::TEXTURE_BINDING,
        });
        let intermediate_view = intermediate.create_view(&TextureViewDescriptor::default());
        let bind_group = device.create_bind_group(&BindGroupDescriptor {
            label: LABEL,
            layout,
            entries: &[
                BindGroupEntry {
                    binding: 0,
                    resource: BindingResource::TextureView(&intermediate_view),
                },
                BindGroupEntry {
                    binding: 1,
                    resource: BindingResource::Sampler(sampler),
                },
            ],
        });
        (intermediate, bind_group)
    }

    fn new(config: SurfaceConfiguration, device: &Device) -> Self {
        let bind_group_layout = device.create_bind_group_layout(&BindGroupLayoutDescriptor {
            label: LABEL,
            entries: &[
                BindGroupLayoutEntry {
                    binding: 0,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Texture {
                        sample_type: TextureSampleType::Float { filterable: false },
                        view_dimension: TextureViewDimension::D2,
                        multisampled: false,
                    },
                    count: None,
                },
                BindGroupLayoutEntry {
                    binding: 1,
                    visibility: ShaderStages::FRAGMENT,
                    ty: BindingType::Sampler(SamplerBindingType::NonFiltering),
                    count: None,
                },
            ],
        });
        let pipeline_layout = device.create_pipeline_layout(&PipelineLayoutDescriptor {
            label: LABEL,
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });
        let buffer = device.create_buffer_init(&BufferInitDescriptor {
            label: LABEL,
            contents: bytemuck::cast_slice(&ViewportCorner::TEXTURE),
            usage: BufferUsages::VERTEX,
        });
        let shader = device.create_shader_module(include_wgsl!("to_srgb.wgsl"));
        let pipeline = device.create_render_pipeline(&RenderPipelineDescriptor {
            label: LABEL,
            layout: Some(&pipeline_layout),
            vertex: VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[ViewportCorner::vertex_buffer_layout()],
            },
            primitive: PrimitiveState {
                topology: PrimitiveTopology::TriangleStrip,
                ..PrimitiveState::default()
            },
            depth_stencil: None,
            multisample: MultisampleState::default(),
            fragment: Some(FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(ColorTargetState {
                    format: config.format,
                    blend: None,
                    write_mask: ColorWrites::ALL,
                })],
            }),
            multiview: None,
        });
        let sampler = device.create_sampler(&SamplerDescriptor {
            label: LABEL,
            address_mode_u: AddressMode::ClampToEdge,
            address_mode_v: AddressMode::ClampToEdge,
            ..SamplerDescriptor::default()
        });
        let (intermediate, bind_group) =
            Self::intermediate(&config, &bind_group_layout, &sampler, device);
        Self {
            config,
            intermediate,
            pipeline,
            buffer,
            sampler,
            bind_group,
            bind_group_layout,
        }
    }
}
