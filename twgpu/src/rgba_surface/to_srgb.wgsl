struct ScreenCorner {
    @location(0) position: vec2<f32>,
    @location(1) tex_coords: vec2<f32>,
};

struct ScreenFragment {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
};

@group(0)
@binding(0)
var tex: texture_2d<f32>;
@group(0)
@binding(1)
var tex_sampler: sampler;

@vertex
fn vs_main(corner: ScreenCorner) -> ScreenFragment {
    var frag: ScreenFragment;
    frag.tex_coords = corner.tex_coords;
    frag.clip_position = vec4<f32>(corner.position, 0., 1.);
    return frag;
}

@fragment
fn fs_main(frag: ScreenFragment) -> @location(0) vec4<f32> {
    let color = textureSample(tex, tex_sampler, frag.tex_coords);
    return vec4<f32>(pow(color.xyz, vec3<f32>(2.2)), color.w);
}
