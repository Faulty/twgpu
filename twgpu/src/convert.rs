use std::convert::TryInto;
use std::fmt;

/// Wrapper around the [`Into`] trait to allow specifying the target type.
/// Meant to replace all safe usages of `as`.
pub trait To {
    fn to<T>(self) -> T
    where
        Self: Into<T> + Sized,
    {
        self.into()
    }
}

impl<T> To for T {}

/// Wrapper around the [`TryInto`] trait to allow specifying the target type.
/// Meant to replace all risky usages of `as`.
/// Floating point numbers are not supported by [`TryInto`].
pub trait As {
    #![allow(non_snake_case)]
    fn AS<T>(self) -> T
    where
        Self: TryInto<T> + Sized,
        <Self as TryInto<T>>::Error: fmt::Debug,
    {
        self.try_into().unwrap()
    }
}

impl<T> As for T {}
