//! This crate provides a [middleware](https://github.com/gfx-rs/wgpu/wiki/Encapsulating-Graphics-Work#middleware-libraries) library for [Teeworlds](https://www.teeworlds.com/) and [DDNet](https://ddnet.tw/) rendering.

mod camera;
mod convert;
/// Map-specific rendering code
pub mod map;
mod rgba_surface;
/// Structs and traits that are helpful in multiple modules
pub mod shared;
mod tw_render_pass;

pub use camera::{Camera, GpuCamera};
pub use convert::{As, To};
pub use rgba_surface::RgbaSurface;
pub use tw_render_pass::TwRenderPass;

const LABEL: Option<&str> = Some("TwGpu");

pub fn device_descriptor(adapter: &wgpu::Adapter) -> wgpu::DeviceDescriptor {
    wgpu::DeviceDescriptor {
        label: LABEL,
        features: FEATURES,
        limits: limits(adapter),
    }
}

/// The set of wgpu optional features that are used: currently none
pub const FEATURES: wgpu::Features = wgpu::Features::empty();

pub fn limits(adapter: &wgpu::Adapter) -> wgpu::Limits {
    let adapter_limits = adapter.limits();
    wgpu::Limits {
        max_texture_dimension_1d: adapter_limits.max_texture_dimension_1d,
        max_texture_dimension_2d: adapter_limits.max_texture_dimension_2d,
        max_texture_dimension_3d: 256,
        max_texture_array_layers: 1,
        max_bind_groups: 1,
        max_dynamic_uniform_buffers_per_pipeline_layout: 0,
        max_dynamic_storage_buffers_per_pipeline_layout: 0,
        max_sampled_textures_per_shader_stage: 2,
        max_samplers_per_shader_stage: 2,
        max_storage_buffers_per_shader_stage: 0,
        max_storage_textures_per_shader_stage: 0,
        max_uniform_buffers_per_shader_stage: 1,
        max_uniform_buffer_binding_size: adapter_limits.max_uniform_buffer_binding_size,
        max_storage_buffer_binding_size: 0,
        max_vertex_buffers: 5,
        // Max layout size of all vertex shader parameters
        max_vertex_attributes: 7,
        max_vertex_buffer_array_stride: 48,
        max_push_constant_size: 0,
        min_uniform_buffer_offset_alignment: 256,
        min_storage_buffer_offset_alignment: 256,
        // Maximum [[location(n)]] (if in the vertex shader, then same as max_vertex_attributes)
        max_inter_stage_shader_components: 8,
        max_compute_workgroup_storage_size: 0,
        max_compute_invocations_per_workgroup: 0,
        max_compute_workgroup_size_x: 0,
        max_compute_workgroup_size_y: 0,
        max_compute_workgroup_size_z: 0,
        max_compute_workgroups_per_dimension: 0,
        max_buffer_size: adapter_limits.max_buffer_size,
    }
}
