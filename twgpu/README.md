TwMap
===

Render [Teeworlds](https://teeworlds.com/) and [DDNet](https://ddnet.tw/) maps anywhere!

This library is written with [wgpu](https://wgpu.rs/), which runs natively on its backends Vulkan, DirectX, Metal, OpenGL and WebGL.

Usage
---

- Add `twgpu = 0.1.0` in your `Cargo.toml`
- Check out the [docs](https://docs.rs/twgpu/latest/twgpu/)

Features
---

- Map rendering is fully implemented
- Tilemap rendering is done with a different method that allows rendering them with the same speed on every zoom level.
The render speed depends on the amount of pixels, however tilemaps that don't cover the whole map are optimized

Limitations
---

- There is no mipmap support as of yet, this means that there are noticable visual artifacts when zooming out
- Tilemaps are uploaded to the gpu as textures, they can't be bigger than the maximum texture size in that context

Upcoming
---

- I have plans to implement mipmaps and rendering of more parts of Teeworlds/DDNet

