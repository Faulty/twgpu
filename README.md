TwGpu
===

This repository contains

- `twgpu`: [Rust](https://www.rust-lang.org/tools/install) library for rendering Teeworlds and DDNet maps
- `twgpu-tools`: Collection of tools that use the library
- `map-inspect-web`: A map preview tool that works in the web by compiling to [webassembly](https://www.rust-lang.org/what/wasm)

For more detailed information, **check out their respective README.md!**

Project Structure
---

The repository is a [cargo workspace](https://doc.rust-lang.org/cargo/reference/workspaces.html) and currently contains 3 different crates.
While it is usually possible to execute all cargo commands from the workspace root, this is not the case for most commands here.
`map-inspect-web` only compiles to wasm and since you usually don't compile to wasm, such commands will fail (see https://github.com/rust-lang/cargo/issues/9406).
Compiled binaries can be found in the workspace root in the `target` directory.

