TwGpu Tools
===

Powered by the [twgpu](https://crates.io/crates/twgpu) library!

Overview
---

All tools are command line argument based and explain their usage with `--help`.

- `twgpu-map-inspect`: View rendered maps in a window
- `twgpu-map-photography`: Snap pictures of maps via the command line

Installation
---

You need [Rust](https://www.rust-lang.org/tools/install) installed on your system.

Simply do `cargo install twgpu-tools`

Manual Building
---

You need [Rust](https://www.rust-lang.org/tools/install) installed on your system.

To compile the tools in release mode, execute the following command in this directory:
```
cargo build --release
```
The executable files will be located in the **project/workspace root** in `target/release/`.

To compile the debug build instead, omit the `--release` flag.
Note that debug binaries will be **significantly slower**.
The executable files of debug builds will be located in `target/debug/`.

