use clap::Parser;
use pollster::FutureExt;
use std::error::Error;
use std::iter;
use std::num::NonZeroU32;
use std::path::PathBuf;
use std::str::FromStr;
use twmap::{CompressedData, LayerKind, LoadMultiple};
use wgpu::{
    Color, CommandEncoderDescriptor, LoadOp, Operations, RenderPassColorAttachment,
    RenderPassDescriptor,
};

use twgpu::map::{GpuMapData, GpuMapStatic};
use twgpu::{device_descriptor, As, Camera, GpuCamera, To, TwRenderPass};

const FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Rgba8Unorm;
const LABEL: Option<&str> = Some("Map Photography");

#[derive(Parser, Debug)]
#[clap(author, version)]
#[clap(
    about = "Take photos of Teeworlds/DDNet maps via the command line. If multiple resolutions/zoom levels/positions are specified, photos of all permutations will be taken"
)]
struct Cli {
    /// Path to the modelling map
    #[clap(value_parser)]
    map: PathBuf,
    /// Path to the external mapres directory
    #[clap(value_parser)]
    mapres_directory: PathBuf,
    /// Zoom levels of the photos, 1 if no zoom level is specified.
    /// 2 -> twice as many tiles on photo, 0.5 -> half as many tiles
    #[clap(long, short = 'z', value_parser)]
    zoom: Vec<f32>,
    /// Resolution <width>x<height> of the taken photos,
    /// 1920x1080 if no resolution is specified
    #[clap(long, short = 'r', value_parser = parse_tuple::<u32, 'x'>)]
    resolution: Vec<(u32, u32)>,
    /// Position <x>,<y> of the camera, 0,0 if no position is specified
    /// 0,0 -> top-left corner of the map, 20,0.5 -> 20 tiles to the right, 0.5 tiles down
    #[clap(long, short = 'p', value_parser = parse_tuple::<f32, ','>)]
    position: Vec<(f32, f32)>,
}

fn parse_tuple<T: FromStr, const SEPARATOR: char>(
    s: &str,
) -> Result<(T, T), Box<dyn Error + Send + Sync + 'static>>
where
    T::Err: Error + Send + Sync + 'static,
{
    if s.matches(SEPARATOR).count() != 1 {
        return Err(format!("Expected 2 values separated by '{}'", SEPARATOR).into());
    }
    let mut values = s.split(SEPARATOR).map(|str| str.parse::<T>());
    Ok((values.next().unwrap()?, values.next().unwrap()?))
}

fn main() -> Result<(), Box<dyn Error>> {
    env_logger::init();
    let mut cli: Cli = Cli::parse();
    let default_resolution = cli.resolution.is_empty();
    if default_resolution {
        cli.resolution.push((1920, 1080));
    }
    let default_zoom = cli.zoom.is_empty();
    if default_zoom {
        cli.zoom.push(1.);
    }

    let mut map = twmap::TwMap::parse_path(&cli.map)?;
    let game_layer = map.find_physics_layer::<twmap::GameLayer>().unwrap();
    let (map_width, map_height) = match &game_layer.tiles {
        CompressedData::Compressed(_, _, info) => (info.width as f32, info.height as f32),
        CompressedData::Loaded(_) => panic!("Game layer should not be loaded"),
    };

    let default_position = cli.position.is_empty();
    if default_position {
        cli.position.push((map_width / 2., map_height / 2.));
    }

    map.embed_images(&cli.mapres_directory)?;
    map.images.load()?;
    map.groups
        .load_conditionally(|layer| layer.kind() == LayerKind::Tiles)?;

    let instance = wgpu::Instance::new(wgpu::Backends::PRIMARY);
    let adapter = instance
        .request_adapter(&wgpu::RequestAdapterOptions {
            power_preference: wgpu::PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: None,
        })
        .block_on()
        .expect("No suitable adapter found");
    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .block_on()?;

    let mut camera = Camera::new(1.);
    let mut gpu_camera = GpuCamera::upload(&camera, &device);
    let map_static = GpuMapStatic::new(FORMAT, &device);
    let map_data = GpuMapData::upload(&map, &device, &queue);
    let map_render = map_static.prepare_render(&map, &map_data, &gpu_camera, &device);
    map_data.envelopes.update(&map.envelopes, 0, 0, &queue);

    for resolution in cli.resolution {
        let download_texture = DownloadTexture::new(resolution.0, resolution.1, &device);
        camera.switch_aspect_ratio(resolution.0 as f32 / resolution.1 as f32);
        for &zoom in &cli.zoom {
            if default_position {
                let width_zoom = map_width / camera.base_dimensions.x;
                let height_zoom = map_height / camera.base_dimensions.y;
                let map_zoom = width_zoom.max(height_zoom);
                camera.zoom = [map_zoom * zoom, map_zoom * zoom].into();
            } else {
                camera.zoom = [zoom, zoom].into();
            }
            for &position in &cli.position {
                camera.position = position.into();
                gpu_camera.update(&camera, &queue);
                let view = download_texture
                    .texture
                    .create_view(&wgpu::TextureViewDescriptor::default());
                let mut command_encoder =
                    device.create_command_encoder(&CommandEncoderDescriptor { label: LABEL });
                {
                    let render_pass = command_encoder.begin_render_pass(&RenderPassDescriptor {
                        label: LABEL,
                        color_attachments: &[Some(RenderPassColorAttachment {
                            view: &view,
                            resolve_target: None,
                            ops: Operations {
                                load: LoadOp::Clear(Color {
                                    r: 0.0,
                                    g: 0.0,
                                    b: 0.0,
                                    a: 1.0,
                                }),
                                store: true,
                            },
                        })],
                        depth_stencil_attachment: None,
                    });
                    let mut tw_render_pass =
                        TwRenderPass::new(render_pass, resolution.into(), &camera);
                    map_render.render(&mut tw_render_pass);
                }
                queue.submit(iter::once(command_encoder.finish()));
                let image = download_texture.download(&device, &queue).block_on()?;
                let mut image_path = cli.map.file_stem().unwrap().to_owned();
                if !default_resolution {
                    image_path.push("_");
                    image_path.push(format!("{}x{}", resolution.0, resolution.1));
                }
                if !default_position {
                    image_path.push("_");
                    image_path.push(format!("{},{}", position.0, position.1));
                }
                if !default_zoom {
                    image_path.push("_");
                    image_path.push(format!("x{}", zoom));
                }
                image_path.push(".png");
                image.save(&image_path)?;
                println!("Saved to {:?}", image_path);
            }
        }
    }
    Ok(())
}

struct DownloadTexture {
    texture: wgpu::Texture,
    download_buffer: wgpu::Buffer,
    width: u32,
    height: u32,
    buffer_size: u64,
    bytes_per_row: u32,
    padded_bytes_per_row: u32,
}

impl DownloadTexture {
    fn new(width: u32, height: u32, device: &wgpu::Device) -> Self {
        assert_ne!(width, 0);
        assert_ne!(height, 0);
        let texture = device.create_texture(&wgpu::TextureDescriptor {
            label: Some("Render Target Texture"),
            size: wgpu::Extent3d {
                width,
                height,
                depth_or_array_layers: 1,
            },
            mip_level_count: 1,
            sample_count: 1,
            dimension: wgpu::TextureDimension::D2,
            format: FORMAT,
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT | wgpu::TextureUsages::COPY_SRC,
        });
        let bytes_per_pixel: u32 = FORMAT.describe().block_size.to();
        let bytes_per_row: u32 = width.to::<u32>() * bytes_per_pixel;
        let align: u32 = wgpu::COPY_BYTES_PER_ROW_ALIGNMENT.to();
        let padding: u32 = (align - (bytes_per_row % align)) % align;
        let padded_bytes_per_row: u32 = bytes_per_row + padding;
        let buffer_size: u64 = padded_bytes_per_row.to::<u64>() * height.to::<u64>();
        let download_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("Image Download Buffer"),
            size: buffer_size,
            usage: wgpu::BufferUsages::MAP_READ | wgpu::BufferUsages::COPY_DST,
            mapped_at_creation: false,
        });
        Self {
            texture,
            download_buffer,
            width,
            height,
            buffer_size,
            bytes_per_row,
            padded_bytes_per_row,
        }
    }

    async fn download(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
    ) -> Result<image::RgbaImage, wgpu::BufferAsyncError> {
        let mut command_encoder = device.create_command_encoder(&wgpu::CommandEncoderDescriptor {
            label: Some("Download Texture Copy"),
        });
        command_encoder.copy_texture_to_buffer(
            self.texture.as_image_copy(),
            wgpu::ImageCopyBuffer {
                buffer: &self.download_buffer,
                layout: wgpu::ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some(NonZeroU32::new(self.padded_bytes_per_row).unwrap()),
                    rows_per_image: Some(NonZeroU32::new(self.height).unwrap()),
                },
            },
            wgpu::Extent3d {
                width: self.width,
                height: self.height,
                depth_or_array_layers: 1,
            },
        );
        queue.submit(Some(command_encoder.finish()));
        let buffer_slice = self.download_buffer.slice(..);
        buffer_slice.map_async(wgpu::MapMode::Read, |e| e.unwrap());
        device.poll(wgpu::Maintain::Wait);
        let mut image_buffer =
            vec![0; self.bytes_per_row.AS::<usize>() * self.height.AS::<usize>()];
        {
            let downloaded = buffer_slice.get_mapped_range();
            assert_eq!(downloaded.len(), self.buffer_size.AS::<usize>());
            downloaded
                .chunks_exact(self.padded_bytes_per_row.AS())
                .zip(image_buffer.chunks_exact_mut(self.bytes_per_row.AS()))
                .for_each(|(downloaded, image)| {
                    image.copy_from_slice(&downloaded[..self.bytes_per_row.AS::<usize>()])
                });
        }
        self.download_buffer.unmap();
        Ok(image::RgbaImage::from_vec(self.width, self.height, image_buffer).unwrap())
    }
}
