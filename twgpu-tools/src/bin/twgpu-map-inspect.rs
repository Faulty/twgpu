use cgmath::Vector2;
use clap::Parser;
use pollster::FutureExt;
use std::path::PathBuf;
use std::time;
use twmap::{LayerKind, LoadMultiple};
use wgpu::{
    Backends, Color, CommandEncoderDescriptor, Instance, LoadOp, Operations, PowerPreference,
    PresentMode, RenderPassColorAttachment, RenderPassDescriptor, RequestAdapterOptions,
    SurfaceConfiguration, TextureFormat, TextureUsages, TextureViewDescriptor,
};
use winit::dpi::PhysicalSize;
use winit::event::{DeviceEvent, Event, MouseScrollDelta, WindowEvent};
use winit::event_loop::ControlFlow;
use winit::{event_loop::EventLoop, window::WindowBuilder};

use twgpu::map::{GpuMapData, GpuMapStatic};
use twgpu::{device_descriptor, Camera, GpuCamera, RgbaSurface, TwRenderPass};

const FORMAT: TextureFormat = TextureFormat::Bgra8Unorm;
const LABEL: Option<&str> = Some("Map Inspect");

#[derive(Parser, Debug)]
#[clap(author, version, about = "View Teeworlds and DDNet maps")]
struct Cli {
    /// Path to the map
    #[clap(value_parser)]
    map: PathBuf,
    /// Path to the external mapres directory
    #[clap(value_parser)]
    mapres_directory: PathBuf,
}

fn main() {
    env_logger::init();
    let cli: Cli = Cli::parse();
    let mut map = twmap::TwMap::parse_path(&cli.map).unwrap();
    map.embed_images(&cli.mapres_directory).unwrap();
    map.images.load().unwrap();
    map.groups
        .load_conditionally(|layer| layer.kind() == LayerKind::Tiles)
        .unwrap();

    let event_loop = EventLoop::new();
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    let instance = Instance::new(Backends::all());
    let surface = unsafe { instance.create_surface(&window) };
    let adapter = instance
        .request_adapter(&RequestAdapterOptions {
            power_preference: PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        })
        .block_on()
        .expect("No suitable adapter found");
    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .block_on()
        .unwrap();

    let PhysicalSize {
        mut width,
        mut height,
    } = window.inner_size();
    let mut config = SurfaceConfiguration {
        usage: TextureUsages::RENDER_ATTACHMENT,
        format: FORMAT,
        width,
        height,
        present_mode: PresentMode::Immediate,
    };
    let mut rgba_surface = RgbaSurface::new(surface, config.clone(), &device);

    let mut camera = Camera::new(width as f32 / height as f32);
    let mut gpu_camera = GpuCamera::upload(&camera, &device);
    let map_static = GpuMapStatic::new(FORMAT, &device);
    let map_data = GpuMapData::upload(&map, &device, &queue);
    let map_render = map_static.prepare_render(&map, &map_data, &gpu_camera, &device);

    eprintln!("Adapter {:#?}", adapter.limits());
    let start = time::Instant::now();

    let mut fps = 0;
    let mut last_fps_stat = time::Instant::now();
    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::DeviceEvent {
                event: device_event,
                ..
            } => match device_event {
                DeviceEvent::MouseMotion { delta: (dx, dy) } => {
                    camera.position += Vector2::new(dx, dy).cast::<f32>().unwrap() / 20.;
                }
                DeviceEvent::MouseWheel {
                    delta: MouseScrollDelta::LineDelta(_, dy),
                } => {
                    if dy.is_sign_positive() {
                        camera.zoom *= 1.1;
                    } else {
                        camera.zoom /= 1.1;
                    }
                }
                _ => {}
            },
            Event::WindowEvent {
                event: window_event,
                ..
            } => match window_event {
                WindowEvent::Resized(size) => {
                    PhysicalSize { width, height } = size;
                    config.width = width;
                    config.height = height;
                    rgba_surface.configure(config.clone(), &device);
                    camera.switch_aspect_ratio(width as f32 / height as f32);
                }
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                if last_fps_stat.elapsed().as_secs() >= 5 {
                    last_fps_stat = time::Instant::now();
                    eprintln!("Fps: {}", fps);
                    fps = 0;
                }
                fps += 1;
                let time = start.elapsed().as_micros() as i64;
                map_data
                    .envelopes
                    .update(&map.envelopes, time, time, &queue);
                gpu_camera.update(&camera, &queue);
                if let Ok(rgba_frame) = rgba_surface.get_current_texture() {
                    let frame_view = rgba_frame
                        .texture()
                        .create_view(&TextureViewDescriptor::default());
                    let mut command_encoder =
                        device.create_command_encoder(&CommandEncoderDescriptor { label: LABEL });
                    {
                        let render_pass =
                            command_encoder.begin_render_pass(&RenderPassDescriptor {
                                label: LABEL,
                                color_attachments: &[Some(RenderPassColorAttachment {
                                    view: &frame_view,
                                    resolve_target: None,
                                    ops: Operations {
                                        load: LoadOp::Clear(Color {
                                            r: 0.0,
                                            g: 0.0,
                                            b: 0.0,
                                            a: 1.0,
                                        }),
                                        store: true,
                                    },
                                })],
                                depth_stencil_attachment: None,
                            });
                        let mut tw_render_pass =
                            TwRenderPass::new(render_pass, Vector2::new(width, height), &camera);
                        map_render.render(&mut tw_render_pass);
                    }
                    if let Ok(surface_texture) =
                        rgba_frame.prepare_present(&rgba_surface, &mut command_encoder)
                    {
                        queue.submit([command_encoder.finish()]);
                        surface_texture.present();
                    }
                }
            }
            _ => (),
        }
    });
}
