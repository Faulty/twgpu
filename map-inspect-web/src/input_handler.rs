use cgmath::{ElementWise, MetricSpace, Point2};
use std::rc::Rc;
use std::sync::Mutex;
use wasm_bindgen::closure::Closure;
use wasm_bindgen::JsCast;
use web_sys::{HtmlCanvasElement, PointerEvent, Touch, TouchEvent};
use winit::window::Window;

use twgpu::Camera;

pub struct InputHandler {
    viewport: Viewport,
    pointer: MultiInput,
    touch: MultiInput,
}

pub struct Viewport {
    pub window: Rc<Window>,
    pub camera: Rc<Mutex<Camera>>,
    pub render_size: Point2<f32>,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum MultiInput {
    None,
    Idle(InputDevice),
    One(InputDevice),
    Two {
        one: InputDevice,
        two: InputDevice,
        distance: f32,
    },
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct InputDevice {
    id: i32,
    logical_pos: Point2<f32>,
    map_pos: Point2<f32>,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Input {
    kind: InputKind,
    id: i32,
    screen_position: Point2<i32>,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum InputKind {
    Pointer,
    Touch,
}

fn render_size() -> Point2<f32> {
    let window = web_sys::window().unwrap();
    Point2::new(window.inner_width(), window.inner_height())
        .map(|n| n.unwrap().as_f64().unwrap())
        .cast::<f32>()
        .unwrap()
}

impl Viewport {
    pub fn new(window: Rc<Window>, camera: Rc<Mutex<Camera>>) -> Self {
        Self {
            window,
            camera,
            render_size: render_size(),
        }
    }
}

impl Input {
    fn from_touch(touch: Touch) -> Self {
        Self {
            kind: InputKind::Touch,
            id: touch.identifier(),
            screen_position: Point2::new(touch.screen_x(), touch.screen_y()),
        }
    }

    fn from_pointer(event: PointerEvent) -> Option<Self> {
        if event.pointer_type() == "touch" {
            None
        } else {
            Some(Self {
                kind: InputKind::Pointer,
                id: event.pointer_id(),
                screen_position: Point2::new(event.x(), event.y()),
            })
        }
    }
}

impl InputDevice {
    fn new(input: &Input, viewport: &Viewport) -> Self {
        let logical_pos = Point2::new(input.screen_position.x, input.screen_position.y)
            .cast::<f32>()
            .unwrap()
            .div_element_wise(viewport.render_size);
        InputDevice {
            id: input.id,
            logical_pos,
            map_pos: viewport.camera.lock().unwrap().map_position(logical_pos),
        }
    }

    fn update(&mut self, input: &Input, viewport: &Viewport) {
        if self.id == input.id {
            self.logical_pos = Point2::new(input.screen_position.x, input.screen_position.y)
                .cast::<f32>()
                .unwrap()
                .div_element_wise(viewport.render_size);
        }
    }

    fn update_map_pos(&mut self, viewport: &Viewport) {
        self.map_pos = viewport
            .camera
            .lock()
            .unwrap()
            .map_position(self.logical_pos);
    }
}

impl MultiInput {
    fn update_camera(&self, camera: &mut Camera) {
        match self {
            MultiInput::None => {}
            MultiInput::Idle(idle) => camera.move_to(idle.map_pos, idle.logical_pos),
            MultiInput::One(only) => camera.move_to(only.map_pos, only.logical_pos),
            MultiInput::Two { one, .. } => camera.move_to(one.map_pos, one.logical_pos),
        }
    }

    fn update_map_pos(&mut self, viewport: &Viewport) {
        match self {
            MultiInput::None => {}
            MultiInput::Idle(idle) => idle.update_map_pos(viewport),
            MultiInput::One(only) => only.update_map_pos(viewport),
            MultiInput::Two { one, two, .. } => {
                one.update_map_pos(viewport);
                two.update_map_pos(viewport);
            }
        }
    }

    fn update_down(&mut self, input: &Input, viewport: &Viewport) {
        match self {
            MultiInput::None | MultiInput::Idle(_) => {
                *self = MultiInput::One(InputDevice::new(input, viewport))
            }
            MultiInput::One(only) => {
                let two = InputDevice::new(input, viewport);
                let distance = only.logical_pos.distance(two.logical_pos);
                *self = MultiInput::Two {
                    one: *only,
                    two,
                    distance,
                }
            }
            MultiInput::Two { .. } => {}
        }
    }

    fn update_move(&mut self, input: &Input, viewport: &Viewport) {
        match self {
            MultiInput::None | MultiInput::Idle(_) => {
                *self = MultiInput::Idle(InputDevice::new(input, viewport))
            }
            MultiInput::One(only) => only.update(input, viewport),
            MultiInput::Two { one, two, distance } => {
                one.update(input, viewport);
                two.update(input, viewport);
                let new_distance = one.logical_pos.distance(two.logical_pos);
                viewport.camera.lock().unwrap().zoom *= *distance / new_distance;
                *distance = new_distance;
            }
        }
    }

    fn update_up(&mut self, input: &Input, viewport: &Viewport) {
        match self {
            MultiInput::None | MultiInput::Idle(_) => {}
            MultiInput::One(only) => {
                if only.id == input.id {
                    *self = MultiInput::Idle(*only);
                }
            }
            MultiInput::Two { one, two, .. } => {
                if one.id == input.id {
                    two.update_map_pos(viewport);
                    *self = MultiInput::One(*two);
                } else if two.id == input.id {
                    *self = MultiInput::One(*one);
                }
            }
        }
    }
}

const POINTER_MAIN_BUTTON: i16 = 0;

impl InputHandler {
    pub fn new(viewport: Viewport) -> Self {
        Self {
            viewport,
            pointer: MultiInput::None,
            touch: MultiInput::None,
        }
    }

    pub fn new_render_size(&mut self) {
        self.viewport.render_size = render_size();
        self.pointer.update_map_pos(&self.viewport);
        self.touch.update_map_pos(&self.viewport);
    }

    pub fn update_camera(&mut self) {
        let mut camera = self.viewport.camera.lock().unwrap();
        if self.pointer != MultiInput::None {
            self.pointer.update_camera(&mut camera);
        } else {
            self.touch.update_camera(&mut camera);
        }
    }

    fn update_down(&mut self, input: &Input) {
        match input.kind {
            InputKind::Pointer => self.pointer.update_down(input, &self.viewport),
            InputKind::Touch => self.touch.update_down(input, &self.viewport),
        }
    }

    fn update_move(&mut self, input: &Input) {
        match input.kind {
            InputKind::Pointer => self.pointer.update_move(input, &self.viewport),
            InputKind::Touch => self.touch.update_move(input, &self.viewport),
        }
        self.update_camera();
    }

    fn update_up(&mut self, input: &Input) {
        self.update_move(input);
        match input.kind {
            InputKind::Pointer => self.pointer.update_up(input, &self.viewport),
            InputKind::Touch => self.touch.update_up(input, &self.viewport),
        }
    }

    pub fn attach(input_handler: Rc<Mutex<InputHandler>>, canvas: Rc<HtmlCanvasElement>) {
        {
            let input_handler = input_handler.clone();
            let pointer_down_closure = Closure::wrap(Box::new(move |e: PointerEvent| {
                if e.button() == POINTER_MAIN_BUTTON {
                    if let Some(input) = Input::from_pointer(e) {
                        input_handler.lock().unwrap().update_down(&input);
                    }
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "pointerdown",
                    pointer_down_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            pointer_down_closure.forget();
        }
        {
            let input_handler = input_handler.clone();
            let pointer_move_closure = Closure::wrap(Box::new(move |e: PointerEvent| {
                if let Some(input) = Input::from_pointer(e) {
                    input_handler.lock().unwrap().update_move(&input);
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "pointermove",
                    pointer_move_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            pointer_move_closure.forget();
        }
        {
            let input_handler = input_handler.clone();
            let pointer_up_closure = Closure::wrap(Box::new(move |e: PointerEvent| {
                if e.button() == POINTER_MAIN_BUTTON {
                    if let Some(input) = Input::from_pointer(e) {
                        input_handler.lock().unwrap().update_up(&input);
                    }
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "pointerup",
                    pointer_up_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            pointer_up_closure.forget();
        }
        {
            let input_handler = input_handler.clone();
            let touch_start_closure = Closure::wrap(Box::new(move |e: TouchEvent| {
                let touches = e.changed_touches();
                for i in 0..touches.length() {
                    let input = Input::from_touch(touches.get(i).unwrap());
                    input_handler.lock().unwrap().update_down(&input);
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "touchstart",
                    touch_start_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            touch_start_closure.forget();
        }
        {
            let input_handler = input_handler.clone();
            let touch_start_closure = Closure::wrap(Box::new(move |e: TouchEvent| {
                let touches = e.changed_touches();
                for i in 0..touches.length() {
                    let input = Input::from_touch(touches.get(i).unwrap());
                    input_handler.lock().unwrap().update_move(&input);
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "touchmove",
                    touch_start_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            touch_start_closure.forget();
        }
        {
            let input_handler = input_handler.clone();
            let touch_start_closure = Closure::wrap(Box::new(move |e: TouchEvent| {
                let touches = e.changed_touches();
                for i in 0..touches.length() {
                    let input = Input::from_touch(touches.get(i).unwrap());
                    input_handler.lock().unwrap().update_up(&input);
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "touchend",
                    touch_start_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            touch_start_closure.forget();
        }
        {
            let input_handler = input_handler.clone();
            let touch_start_closure = Closure::wrap(Box::new(move |e: TouchEvent| {
                let touches = e.changed_touches();
                for i in 0..touches.length() {
                    let input = Input::from_touch(touches.get(i).unwrap());
                    input_handler.lock().unwrap().update_up(&input);
                }
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "touchcancel",
                    touch_start_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            touch_start_closure.forget();
        }
        {
            let window = input_handler.lock().unwrap().viewport.window.clone();
            let rc_canvas = canvas.clone();
            let double_click_closure = Closure::wrap(Box::new(move |e: PointerEvent| {
                if window.fullscreen().is_none() {
                    // TODO: call fullscreen on winit Window, once that doesn't cause delay
                    // before, it required three click on native to get the fullscreen effect
                    rc_canvas.request_fullscreen().ok();
                } else {
                    window.set_fullscreen(None);
                }
                e.prevent_default();
            }) as Box<dyn FnMut(_)>);
            canvas
                .add_event_listener_with_callback(
                    "dblclick",
                    double_click_closure.as_ref().unchecked_ref(),
                )
                .unwrap();
            double_click_closure.forget();
        }
    }
}
