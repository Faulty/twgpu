use cgmath::{Point2, Vector2};
use image::codecs::png::PngDecoder;
use image::{ColorType, ImageDecoder, RgbaImage};
use std::rc::Rc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Mutex;
use twmap::{EmbeddedImage, GameLayer, Image, LayerKind, LoadMultiple, TwMap, Version};
use wasm_bindgen::prelude::*;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use web_sys::{HtmlCanvasElement, Response};
use wgpu::{
    Backends, Color, CommandEncoderDescriptor, Instance, LoadOp, Operations, PowerPreference,
    PresentMode, RenderPassColorAttachment, RenderPassDescriptor, RequestAdapterOptions,
    SurfaceConfiguration, TextureUsages, TextureViewDescriptor,
};
use winit::dpi::{LogicalSize, PhysicalPosition, PhysicalSize};
use winit::event::{Event, MouseScrollDelta, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::platform::web::WindowExtWebSys;
use winit::window::Window;

use twgpu::map::{GpuMapData, GpuMapStatic};
use twgpu::{device_descriptor, Camera, GpuCamera, RgbaSurface, TwRenderPass};

mod input_handler;
use input_handler::{InputHandler, Viewport};

const LABEL: Option<&str> = Some("Map Inspect Web");

async fn fetch_data(url: &str) -> Result<Vec<u8>, JsValue> {
    let window = web_sys::window().unwrap();
    let response = JsFuture::from(window.fetch_with_str(url)).await?;
    assert!(response.is_instance_of::<Response>());
    let response: Response = response.dyn_into()?;
    let js_array = JsFuture::from(response.array_buffer().unwrap()).await?;
    Ok(js_sys::Uint8Array::new(&js_array).to_vec())
}

async fn load_external_image(image: &mut Image, version: Version) -> Result<(), JsValue> {
    if let Image::External(ex) = image {
        let version = match version {
            Version::DDNet06 => "06",
            Version::Teeworlds07 => "07",
        };
        let url = format!("mapres_{}/{}.png", version, ex.name);
        let data = fetch_data(&url).await?;
        let image_decoder = PngDecoder::new(data.as_slice()).unwrap();
        assert_eq!(image_decoder.color_type(), ColorType::Rgba8);
        let mut image_buffer = vec![0_u8; image_decoder.total_bytes() as usize];
        let (width, height) = image_decoder.dimensions();
        image_decoder.read_image(&mut image_buffer).unwrap();
        let rgba_image = RgbaImage::from_vec(width, height, image_buffer).unwrap();
        *image = Image::Embedded(EmbeddedImage {
            name: image.name().clone(),
            image: rgba_image.into(),
        });
    }
    Ok(())
}

fn window_size() -> PhysicalSize<f64> {
    let window = web_sys::window().unwrap();
    let logical_size = LogicalSize::new(
        window.inner_width().unwrap().as_f64().unwrap(),
        window.inner_height().unwrap().as_f64().unwrap(),
    );
    let scale_factor = window.device_pixel_ratio();
    let physical_size = logical_size.to_physical(scale_factor);
    let screen = window.screen().unwrap();
    let screen_size = scale_factor
        * Vector2::new(screen.avail_width(), screen.avail_height())
            .map(|res| res.expect("Screen size not available"))
            .cast::<f64>()
            .unwrap();
    log::info!(
        "Determined screen size: {}x{} with scale factor {}",
        screen_size.x,
        screen_size.y,
        scale_factor
    );
    if physical_size.width > screen_size.x as f64 || physical_size.height > screen_size.y as f64 {
        log::info!(
            "Logical size: {}x{}, Physical size: {}x{}, using logical (for performance)",
            logical_size.width,
            logical_size.height,
            physical_size.width,
            physical_size.height
        );
        logical_size.to_physical(1.)
    } else {
        log::info!(
            "Canvas logical size: {}x{}, physical size: {}x{}, using physical (per default)",
            logical_size.width,
            logical_size.height,
            physical_size.width,
            physical_size.height
        );
        physical_size
    }
}

#[wasm_bindgen(start)]
pub async fn main() -> Result<(), JsValue> {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init().expect("Could not initialize logger");

    let doc = web_sys::window()
        .and_then(|win| win.document())
        .expect("No document in index.js");
    let event_loop = EventLoop::new();
    let window = winit::window::WindowBuilder::new()
        .with_inner_size(window_size())
        .build(&event_loop)
        .unwrap();
    let canvas = window.canvas();
    canvas
        .set_attribute("style", "width: 100%; height: 100%;")
        .unwrap();
    doc.body()
        .expect("No body in document")
        .append_child(&canvas)
        .unwrap();
    let search_params = web_sys::Url::new(&doc.url()?)?.search_params();
    let mut map = None;
    if let Some(map_name) = search_params.get("map") {
        let data = fetch_data(&format!("maps/{}.map", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(map_name) = search_params.get("ddnet") {
        let data = fetch_data(&format!("https://ddnet.tw/mappreview/{}.map", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(map_name) = search_params.get("ddnet-testing") {
        let data = fetch_data(&format!("https://ddnet.tw/testmaps/{}.map", map_name)).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?)
    }
    if let Some(url) = search_params.get("url") {
        let data = fetch_data(&url).await?;
        map = Some(TwMap::parse(&data).map_err(|err| err.to_string())?);
    }
    let mut map = match map {
        Some(map) => map,
        None => {
            let data = fetch_data("maps/default.map").await?;
            TwMap::parse(&data).map_err(|err| err.to_string())?
        }
    };

    map.images.load().map_err(|err| err.to_string())?;
    // Tiles layer for the rendering
    // Game layer for the camera start position
    map.groups
        .load_conditionally(|layer| [LayerKind::Tiles, LayerKind::Game].contains(&layer.kind()))
        .map_err(|err| err.to_string())?;
    futures::future::join_all(
        map.images
            .iter_mut()
            .map(|image| load_external_image(image, map.version)),
    )
    .await
    .into_iter()
    .collect::<Result<_, _>>()?;
    wasm_bindgen_futures::spawn_local(run(window, event_loop, map, canvas));
    Ok(())
}

fn start_position(map: &mut TwMap) -> Point2<f32> {
    let game_tiles = map
        .find_physics_layer::<GameLayer>()
        .unwrap()
        .tiles
        .unwrap_ref();
    let mut spawns_sum = Point2::new(0., 0.);
    let spawns_amount = game_tiles
        .indexed_iter()
        .filter(|(_, tile)| (192..=194).contains(&tile.id))
        .map(|((y, x), _)| spawns_sum += Vector2::new(x as f32, y as f32))
        .count();
    if spawns_amount == 0 {
        Point2::new(0., 0.)
    } else {
        spawns_sum / spawns_amount as f32
    }
}

static RESIZED: AtomicBool = AtomicBool::new(false);

async fn run(window: Window, event_loop: EventLoop<()>, mut map: TwMap, canvas: HtmlCanvasElement) {
    let PhysicalSize {
        mut width,
        mut height,
    } = window.inner_size();
    let mut camera = Camera::new(width as f32 / height as f32);
    camera.position = start_position(&mut map);
    let camera = Rc::new(Mutex::new(camera));

    let instance = Instance::new(Backends::GL);
    let surface = unsafe { instance.create_surface(&window) };
    let adapter = instance
        .request_adapter(&RequestAdapterOptions {
            power_preference: PowerPreference::HighPerformance,
            force_fallback_adapter: false,
            compatible_surface: Some(&surface),
        })
        .await
        .expect("No suitable adapter found");
    let (device, queue) = adapter
        .request_device(&device_descriptor(&adapter), None)
        .await
        .unwrap();

    let format = *surface
        .get_supported_formats(&adapter)
        .get(0)
        .expect("No preferred texture format given");
    let mut config = SurfaceConfiguration {
        usage: TextureUsages::RENDER_ATTACHMENT,
        format,
        width,
        height,
        present_mode: PresentMode::Fifo,
    };
    let mut rgba_surface = RgbaSurface::new(surface, config.clone(), &device);

    let mut gpu_camera = GpuCamera::upload(&camera.lock().unwrap(), &device);
    let map_static = GpuMapStatic::new(format, &device);
    let map_data = GpuMapData::upload(&map, &device, &queue);
    let map_render = map_static.prepare_render(&map, &map_data, &gpu_camera, &device);

    log::info!("Adapter {:?}", adapter.limits());
    let window = Rc::new(window);
    {
        let window = window.clone();
        let resize_closure = Closure::wrap(Box::new(move |_e: web_sys::Event| {
            let size = window_size();
            window.set_inner_size(size);
            RESIZED.store(true, Ordering::Relaxed);
        }) as Box<dyn FnMut(_)>);
        web_sys::window()
            .unwrap()
            .add_event_listener_with_callback("resize", resize_closure.as_ref().unchecked_ref())
            .unwrap();
        resize_closure.forget();
    }
    let perf_timer = web_sys::window()
        .expect("Web Sys can't find a window")
        .performance()
        .expect("Web Sys window doesnt have performance");

    let mut fps = 0;
    let mut last_fps_stat = perf_timer.now();

    let canvas = Rc::new(canvas);
    let input_handler = Rc::new(Mutex::new(InputHandler::new(Viewport::new(
        window.clone(),
        camera.clone(),
    ))));
    InputHandler::attach(input_handler.clone(), canvas.clone());

    event_loop.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;

        match event {
            Event::WindowEvent {
                event: window_event,
                ..
            } => match window_event {
                /* not supported yet by winit
                WindowEvent::Resized(size) => {
                    gpu.update_render_size(size);
                    camera.base_width =
                        DEFAULT_TILES_VERTICAL / size.height as f32 * size.width as f32;
                }
                */
                WindowEvent::MouseWheel { delta, .. } => {
                    let zoom_out = match delta {
                        MouseScrollDelta::LineDelta(_, dy) => dy.is_sign_positive(),
                        MouseScrollDelta::PixelDelta(PhysicalPosition { y, .. }) => {
                            y.is_sign_positive()
                        }
                    };
                    if zoom_out {
                        camera.lock().unwrap().zoom /= 1.1;
                    } else {
                        camera.lock().unwrap().zoom *= 1.1;
                    }
                    input_handler.lock().unwrap().update_camera();
                }
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => {}
            },
            Event::MainEventsCleared => {
                window.request_redraw();
            }
            Event::RedrawRequested(_) => {
                if RESIZED.swap(false, Ordering::Relaxed) {
                    canvas
                        .set_attribute("style", "width: 100%; height: 100%;")
                        .unwrap();
                    PhysicalSize { width, height } = window.inner_size();
                    config.width = width;
                    config.height = height;
                    rgba_surface.configure(config.clone(), &device);
                    input_handler.lock().unwrap().new_render_size();
                    camera
                        .lock()
                        .unwrap()
                        .switch_aspect_ratio(width as f32 / height as f32);
                }
                // Only write fps once every 5 seconds
                if perf_timer.now() - last_fps_stat >= 5000. {
                    last_fps_stat = perf_timer.now();
                    log::info!("Fps: {}", fps as f32 / 5.);
                    fps = 0;
                }
                fps += 1;
                let time = (perf_timer.now() * 1000.) as i64;
                map_data
                    .envelopes
                    .update(&map.envelopes, time, time, &queue);
                let render_camera = *camera.lock().unwrap();
                gpu_camera.update(&render_camera, &queue);
                if let Ok(rgba_frame) = rgba_surface.get_current_texture() {
                    let frame_view = rgba_frame
                        .texture()
                        .create_view(&TextureViewDescriptor::default());
                    let mut command_encoder =
                        device.create_command_encoder(&CommandEncoderDescriptor { label: LABEL });
                    {
                        let render_pass =
                            command_encoder.begin_render_pass(&RenderPassDescriptor {
                                label: LABEL,
                                color_attachments: &[Some(RenderPassColorAttachment {
                                    view: &frame_view,
                                    resolve_target: None,
                                    ops: Operations {
                                        load: LoadOp::Clear(Color {
                                            r: 0.0,
                                            g: 0.0,
                                            b: 0.0,
                                            a: 1.0,
                                        }),
                                        store: true,
                                    },
                                })],
                                depth_stencil_attachment: None,
                            });
                        let mut tw_render_pass = TwRenderPass::new(
                            render_pass,
                            Vector2::new(width, height),
                            &render_camera,
                        );
                        map_render.render(&mut tw_render_pass);
                    }
                    if let Ok(surface_texture) =
                        rgba_frame.prepare_present(&rgba_surface, &mut command_encoder)
                    {
                        queue.submit([command_encoder.finish()]);
                        surface_texture.present();
                    }
                }
            }
            _ => (),
        }
    });
}
